<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectsDevices
 *
 * @ORM\Table(name="projects_devices", indexes={@ORM\Index(name="index_project_id", columns={"project_id"}), @ORM\Index(name="index_device_id", columns={"device_id"})})
 * @ORM\Entity
 */
class ProjectsDevices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="recycled", type="boolean", nullable=true)
     */
    private $recycled;

    /**
     * @var \Devices
     *
     * @ORM\ManyToOne(targetEntity="Devices")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="device_id", referencedColumnName="id")
     * })
     */
    private $device;

    /**
     * @var \Projects
     *
     * @ORM\ManyToOne(targetEntity="Projects")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recycled
     *
     * @param boolean $recycled
     * @return ProjectsDevices
     */
    public function setRecycled($recycled)
    {
        $this->recycled = $recycled;

        return $this;
    }

    /**
     * Get recycled
     *
     * @return boolean 
     */
    public function getRecycled()
    {
        return $this->recycled;
    }

    /**
     * Set device
     *
     * @param \Devices $device
     * @return ProjectsDevices
     */
    public function setDevice(\Devices $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \Devices 
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set project
     *
     * @param \Projects $project
     * @return ProjectsDevices
     */
    public function setProject(\Projects $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Projects 
     */
    public function getProject()
    {
        return $this->project;
    }
}
