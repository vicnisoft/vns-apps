<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Devices
 *
 * @ORM\Table(name="devices")
 * @ORM\Entity
 */
class Devices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="device_name", type="string", length=255, nullable=true)
     */
    private $deviceName;

    /**
     * @var string
     *
     * @ORM\Column(name="device_model", type="string", length=255, nullable=true)
     */
    private $deviceModel;

    /**
     * @var string
     *
     * @ORM\Column(name="system_name", type="string", length=255, nullable=true)
     */
    private $systemName;

    /**
     * @var string
     *
     * @ORM\Column(name="system_version", type="string", length=255, nullable=true)
     */
    private $systemVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="carrier_name", type="string", length=255, nullable=true)
     */
    private $carrierName;

    /**
     * @var string
     *
     * @ORM\Column(name="carrier_country", type="string", length=255, nullable=true)
     */
    private $carrierCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="carrier_iso_country_code", type="string", length=255, nullable=true)
     */
    private $carrierIsoCountryCode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="jailbroken", type="boolean", nullable=true)
     */
    private $jailbroken;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=255, nullable=true)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="timezone_ss", type="string", length=255, nullable=true)
     */
    private $timezoneSs;

    /**
     * @var string
     *
     * @ORM\Column(name="unique_id", type="string", length=255, nullable=true)
     */
    private $uniqueId;

    /**
     * @var string
     *
     * @ORM\Column(name="device_signature", type="string", length=255, nullable=true)
     */
    private $deviceSignature;

    /**
     * @var string
     *
     * @ORM\Column(name="cfuuid", type="string", length=255, nullable=true)
     */
    private $cfuuid;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_on", type="integer", nullable=true)
     */
    private $createdOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_on", type="integer", nullable=true)
     */
    private $updatedOn;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deviceName
     *
     * @param string $deviceName
     * @return Devices
     */
    public function setDeviceName($deviceName)
    {
        $this->deviceName = $deviceName;

        return $this;
    }

    /**
     * Get deviceName
     *
     * @return string 
     */
    public function getDeviceName()
    {
        return $this->deviceName;
    }

    /**
     * Set deviceModel
     *
     * @param string $deviceModel
     * @return Devices
     */
    public function setDeviceModel($deviceModel)
    {
        $this->deviceModel = $deviceModel;

        return $this;
    }

    /**
     * Get deviceModel
     *
     * @return string 
     */
    public function getDeviceModel()
    {
        return $this->deviceModel;
    }

    /**
     * Set systemName
     *
     * @param string $systemName
     * @return Devices
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * Get systemName
     *
     * @return string 
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * Set systemVersion
     *
     * @param string $systemVersion
     * @return Devices
     */
    public function setSystemVersion($systemVersion)
    {
        $this->systemVersion = $systemVersion;

        return $this;
    }

    /**
     * Get systemVersion
     *
     * @return string 
     */
    public function getSystemVersion()
    {
        return $this->systemVersion;
    }

    /**
     * Set carrierName
     *
     * @param string $carrierName
     * @return Devices
     */
    public function setCarrierName($carrierName)
    {
        $this->carrierName = $carrierName;

        return $this;
    }

    /**
     * Get carrierName
     *
     * @return string 
     */
    public function getCarrierName()
    {
        return $this->carrierName;
    }

    /**
     * Set carrierCountry
     *
     * @param string $carrierCountry
     * @return Devices
     */
    public function setCarrierCountry($carrierCountry)
    {
        $this->carrierCountry = $carrierCountry;

        return $this;
    }

    /**
     * Get carrierCountry
     *
     * @return string 
     */
    public function getCarrierCountry()
    {
        return $this->carrierCountry;
    }

    /**
     * Set carrierIsoCountryCode
     *
     * @param string $carrierIsoCountryCode
     * @return Devices
     */
    public function setCarrierIsoCountryCode($carrierIsoCountryCode)
    {
        $this->carrierIsoCountryCode = $carrierIsoCountryCode;

        return $this;
    }

    /**
     * Get carrierIsoCountryCode
     *
     * @return string 
     */
    public function getCarrierIsoCountryCode()
    {
        return $this->carrierIsoCountryCode;
    }

    /**
     * Set jailbroken
     *
     * @param boolean $jailbroken
     * @return Devices
     */
    public function setJailbroken($jailbroken)
    {
        $this->jailbroken = $jailbroken;

        return $this;
    }

    /**
     * Get jailbroken
     *
     * @return boolean 
     */
    public function getJailbroken()
    {
        return $this->jailbroken;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Devices
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set language
     *
     * @param string $language
     * @return Devices
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set timezoneSs
     *
     * @param string $timezoneSs
     * @return Devices
     */
    public function setTimezoneSs($timezoneSs)
    {
        $this->timezoneSs = $timezoneSs;

        return $this;
    }

    /**
     * Get timezoneSs
     *
     * @return string 
     */
    public function getTimezoneSs()
    {
        return $this->timezoneSs;
    }

    /**
     * Set uniqueId
     *
     * @param string $uniqueId
     * @return Devices
     */
    public function setUniqueId($uniqueId)
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    /**
     * Get uniqueId
     *
     * @return string 
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * Set deviceSignature
     *
     * @param string $deviceSignature
     * @return Devices
     */
    public function setDeviceSignature($deviceSignature)
    {
        $this->deviceSignature = $deviceSignature;

        return $this;
    }

    /**
     * Get deviceSignature
     *
     * @return string 
     */
    public function getDeviceSignature()
    {
        return $this->deviceSignature;
    }

    /**
     * Set cfuuid
     *
     * @param string $cfuuid
     * @return Devices
     */
    public function setCfuuid($cfuuid)
    {
        $this->cfuuid = $cfuuid;

        return $this;
    }

    /**
     * Get cfuuid
     *
     * @return string 
     */
    public function getCfuuid()
    {
        return $this->cfuuid;
    }

    /**
     * Set createdOn
     *
     * @param integer $createdOn
     * @return Devices
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return integer 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set updatedOn
     *
     * @param integer $updatedOn
     * @return Devices
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * Get updatedOn
     *
     * @return integer 
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }
}
