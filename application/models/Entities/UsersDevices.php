<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * UsersDevices
 *
 * @ORM\Table(name="users_devices", indexes={@ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="device_id", columns={"device_id"})})
 * @ORM\Entity
 */
class UsersDevices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="recycled", type="boolean", nullable=true)
     */
    private $recycled;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \Devices
     *
     * @ORM\ManyToOne(targetEntity="Devices")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="device_id", referencedColumnName="id")
     * })
     */
    private $device;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recycled
     *
     * @param boolean $recycled
     * @return UsersDevices
     */
    public function setRecycled($recycled)
    {
        $this->recycled = $recycled;

        return $this;
    }

    /**
     * Get recycled
     *
     * @return boolean 
     */
    public function getRecycled()
    {
        return $this->recycled;
    }

    /**
     * Set user
     *
     * @param \Users $user
     * @return UsersDevices
     */
    public function setUser(\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Users 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set device
     *
     * @param \Devices $device
     * @return UsersDevices
     */
    public function setDevice(\Devices $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \Devices 
     */
    public function getDevice()
    {
        return $this->device;
    }
}
