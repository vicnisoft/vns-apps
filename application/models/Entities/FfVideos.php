<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * FfVideos
 *
 * @ORM\Table(name="ff_videos", indexes={@ORM\Index(name="index_category_id", columns={"category_id"}), @ORM\Index(name="index_inspector_id", columns={"inspector_id"}), @ORM\Index(name="index_created_by", columns={"created_by_id"}), @ORM\Index(name="index_updated_by", columns={"updated_by_id"})})
 * @ORM\Entity
 */
class FfVideos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="icon_url", type="string", length=255, nullable=false)
     */
    private $iconUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="audio_url", type="string", length=255, nullable=false)
     */
    private $audioUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube_id", type="string", length=255, nullable=true)
     */
    private $youtubeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_on", type="integer", nullable=false)
     */
    private $createdOn;

    /**
     * @var boolean
     *
     * @ORM\Column(name="recycled", type="boolean", nullable=true)
     */
    private $recycled;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_on", type="integer", nullable=true)
     */
    private $updatedOn;

    /**
     * @var boolean
     *
     * @ORM\Column(name="approved", type="boolean", nullable=true)
     */
    private $approved;

    /**
     * @var \FfCategories
     *
     * @ORM\ManyToOne(targetEntity="FfCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inspector_id", referencedColumnName="id")
     * })
     */
    private $inspector;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by_id", referencedColumnName="id")
     * })
     */
    private $updatedBy;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FfVideos
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return FfVideos
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set iconUrl
     *
     * @param string $iconUrl
     * @return FfVideos
     */
    public function setIconUrl($iconUrl)
    {
        $this->iconUrl = $iconUrl;

        return $this;
    }

    /**
     * Get iconUrl
     *
     * @return string 
     */
    public function getIconUrl()
    {
        return $this->iconUrl;
    }

    /**
     * Set audioUrl
     *
     * @param string $audioUrl
     * @return FfVideos
     */
    public function setAudioUrl($audioUrl)
    {
        $this->audioUrl = $audioUrl;

        return $this;
    }

    /**
     * Get audioUrl
     *
     * @return string 
     */
    public function getAudioUrl()
    {
        return $this->audioUrl;
    }

    /**
     * Set youtubeId
     *
     * @param string $youtubeId
     * @return FfVideos
     */
    public function setYoutubeId($youtubeId)
    {
        $this->youtubeId = $youtubeId;

        return $this;
    }

    /**
     * Get youtubeId
     *
     * @return string 
     */
    public function getYoutubeId()
    {
        return $this->youtubeId;
    }

    /**
     * Set createdOn
     *
     * @param integer $createdOn
     * @return FfVideos
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return integer 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set recycled
     *
     * @param boolean $recycled
     * @return FfVideos
     */
    public function setRecycled($recycled)
    {
        $this->recycled = $recycled;

        return $this;
    }

    /**
     * Get recycled
     *
     * @return boolean 
     */
    public function getRecycled()
    {
        return $this->recycled;
    }

    /**
     * Set updatedOn
     *
     * @param integer $updatedOn
     * @return FfVideos
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * Get updatedOn
     *
     * @return integer 
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     * @return FfVideos
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean 
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set category
     *
     * @param \FfCategories $category
     * @return FfVideos
     */
    public function setCategory(\FfCategories $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \FfCategories 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set createdBy
     *
     * @param \Users $createdBy
     * @return FfVideos
     */
    public function setCreatedBy(\Users $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Users 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set inspector
     *
     * @param \Users $inspector
     * @return FfVideos
     */
    public function setInspector(\Users $inspector = null)
    {
        $this->inspector = $inspector;

        return $this;
    }

    /**
     * Get inspector
     *
     * @return \Users 
     */
    public function getInspector()
    {
        return $this->inspector;
    }

    /**
     * Set updatedBy
     *
     * @param \Users $updatedBy
     * @return FfVideos
     */
    public function setUpdatedBy(\Users $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Users 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}
