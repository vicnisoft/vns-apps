<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 2/22/16
 * Time: 11:53 PM
 */

include_once 'Entities/FfCategories.php';

class FFCategories_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
    }


    public function getCategoryWithArgs($args){
        $newObj = new FfCategories();
        $newObj->setName(trim($args->name));
        $newObj->setDescription(trim($args->desc));
        $newObj->setIconUrl(trim($args->thumbnailUrl));
        $newObj->setCreatedOn(now());
        $newObj->setRecycled(false);

        if($this->findByName($args->name) === null){
            $this->em->persist($newObj);
            $this->em->flush();
        }

        return $this->toArray($newObj);
    }
    public function findAllKeyAndValueOrderBy($orderBy = null, $asc = true){
        $categoryArray = null;
        if($orderBy != null){
            $ascending = ($asc == true) ? 'asc' : 'desc';
            $categoryArray= $this->em->getRepository('FfCategories')->findBy(array('recycled' => 0), array($orderBy=>$ascending));
        } else {
            $categoryArray= $this->em->getRepository('FfCategories')->findAll();

        }
        $result = array();
        foreach($categoryArray as $category){
            $result[] = $this->toKeyValueArray($category);
        }
        return $result;
    }

    public function findById($id = null){
        return  $this->em->find('FfCategories', $id);

    }

    public function findByName($name= ''){
        return $this->em->getRepository('FfCategories')->findOneByName($name);
    }

    public function findAllCategoriesOrderBy($orderBy = null, $asc = true){
        $objs = null;
        if($orderBy != null){
            $ascending = ($asc == true) ? 'asc' : 'desc';
            $objs= $this->em->getRepository('FfCategories')->findBy(array('recycled' => 0), array($orderBy=>$ascending));
        } else {
            $objs= $this->em->getRepository('FfCategories')->findAll();

        }

        return $objs;
    }

    public function insert($name = '', $icon = '', $desc = ''){
        try{
            $this->em->getConnection()->beginTransaction();
            $newObj = new FfCategories();
            $newObj->setName(trim($name));
            $newObj->setIconUrl(trim($icon));
            $newObj->setDescription(trim($desc));
            $newObj->setCreatedOn(now());
            $newObj->setRecycled(false);
            $this->em->persist($newObj);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function update($id = null,$name = '', $icon = '', $desc = ''){
        if($id == null){
            return false;
        }
        try{
            $this->em->getConnection()->beginTransaction();
            $anObj = $this->em->find('FfCategories', $id);
            $anObj->setName(trim($name));
            $anObj->setIconUrl(trim($icon));
            $anObj->setDescription(trim($desc));
            $this->em->persist($anObj);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function  delete($id = null){
        if($id == null){
            return false;
        }
        try{
            $this->em->getConnection()->beginTransaction();
            $anObj = $this->em->find('FfCategories', $id);
            $anObj->setRecycled(true);
            $this->em->persist($anObj);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function  deleteForever($id = null){
        if($id == null){
            return false;
        }
        try{
            $this->em->getConnection()->beginTransaction();
            $anObj = $this->em->find('FfCategories', $id);
            $this->em->remove($anObj);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }


    private function toArray(FfCategories $obj){
        $anObj = array();
        $anObj['id'] = $obj->getId();
        $anObj['name'] = $obj->getName();
        $anObj['description'] = $obj->getDescription();
        $anObj['short_description'] = substr($obj->getDescription(),0,100);
        $anObj['created_on'] = convert_timestampseconds_to_date_string($obj->getCreatedOn());
        $anObj['icon'] = $obj->getIconUrl();
        $anObj['recycled'] = $obj->getRecycled();
        $anObj['edit_url'] = array('category','edit',$anObj['id']);

        return $anObj;
    }

    public function toKeyValueArray(FfCategories $obj){
        $anObj = array();
        $anObj['value'] = $obj->getId();
        $anObj['key'] = $obj->getName();

        return $anObj;
    }

}