<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 1/29/16
 * Time: 4:26 PM
 */



// any_in_array() is not in the Array Helper, so it defines a new function
function any_in_array($needle, $haystack)
{
    $needle = is_array($needle) ? $needle : array($needle);

    foreach ($needle as $item)
    {
        if (in_array($item, $haystack))
        {
            return TRUE;
        }
    }

    return FALSE;
}

// random_element() is included in Array Helper, so it overrides the native function
function random_element($array)
{
    shuffle($array);
    return array_pop($array);
}

function reduce_use_info($user){
    $aUser['id'] = $user->id;
    $aUser['username'] = $user->username;
    $aUser['email'] = $user->email;
    $aUser['first_name'] = $user->first_name;
    $aUser['last_name'] = $user->last_name;
    $aUser['company'] = $user->company;
    $aUser['phone'] = $user->phone;
    $aUser['created_date'] = convert_timestampseconds_to_date_string($user->created_on);
    $aUser['edit_url'] = array('user','edit',$user->id);
    return $aUser;
}

function convert_timestampseconds_to_date_string($seconds){

    $datestring = '%d-%m-%Y %h:%i %a';
    $time = time($seconds);
    return mdate($datestring, $time);
}

function objectToArray($object){
    $array = json_decode(json_encode($object), true);
    return $array;
}
