<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 1/28/16
 * Time: 2:18 AM
 */


class MY_Controller extends CI_Controller
{
    protected $em;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language'));
        $this->lang->load('auth');
        $this->load->model('user_model');
        $this->em = $this->doctrine->em;

        $this->authorization_checker();

    }
    private function authorization_checker(){

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect(site_url('auth/login'), 'refresh');
        }
//        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
//        {
//            $this->data['user'] = $this->ion_auth->user()->row();
//        }
        else
        {
            $this->data['user'] = $this->ion_auth->user()->row();
            $this->data['project_name'] = PROJECT_NAME;
            $this->data['title'] = PROJECT_NAME;
            $datestring = "%d/%m/%Y - %h:%i %a";
            $time = time();
            $this->data['time'] = mdate($datestring, $time);;

        }
    }

    /**
     * @return Users
     */
    protected function getUser(){
        return $this->em->find('Users', $this->ion_auth->user()->row()->id);
    }

}