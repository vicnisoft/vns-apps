<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 2/18/16
 * Time: 10:59 PM
 */


class Tools extends  MY_Controller
{


    function __construct(){
        parent::__construct();
    }
    public function index() {

        $url = $this->input->post('tool_url');
        if($url){

            $curlResource = curl_init($url);
            curl_setopt($curlResource, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curlResource, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curlResource, CURLOPT_AUTOREFERER, true);

            $page = curl_exec($curlResource);
            curl_close($curlResource);


            $domDocument = new DOMDocument();
            /* To disable warning OMDocument::loadHTML error*/
            libxml_use_internal_errors(true);
            $domDocument->loadHTML($page);
            libxml_use_internal_errors(false);

            $xpath = new DOMXPath($domDocument);

            $urlXpath = $xpath->query("//img/@src");

            $images = array();
            for($i = 0; $i < $urlXpath->length; $i++){
                $images[] = $urlXpath->item($i)->nodeValue;
            }

        }
        $this->data['page_title'] = "Tools";
        $this->twig->display('twig/pages/tools', $this->data);
    }



    function generate_classes()
    {
        try{
            $this->em->getConfiguration()
                ->setMetadataDriverImpl(
                    new \Doctrine\ORM\Mapping\Driver\DatabaseDriver(
                        $this->em->getConnection()->getSchemaManager()
                    )
                );
            $cmf = new \Doctrine\ORM\Tools\DisconnectedClassMetadataFactory();
            $cmf->setEntityManager($this->em);
            $metadata = $cmf->getAllMetadata();
            $generator = new \Doctrine\ORM\Tools\EntityGenerator();

            $generator->setUpdateEntityIfExists(true);
            $generator->setGenerateStubMethods(true);
            $generator->setGenerateAnnotations(true);
            $generator->generate($metadata, APPPATH."models/Entities");
            $this->data['success_masage'] = "All entities have been generated";

        } catch(Exception $e){
            $this->data['error_message'] = $e->getMessage();

        }
        $this->data['page_title'] = "Tools";
        $this->twig->display('twig/pages/tools', $this->data);

    }

}