<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 2/26/16
 * Time: 10:58 AM
 */
class Dialogue extends MY_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('ffdialogues_model');
        $this->load->model('ffcategories_model');

    }


    public function index() {

        $objs = $this->ffdialogues_model->findAllDialoguesOrderBy('name', true);
        $this->data['dialogues'] = $objs;

        $data = array(
            'recycle_action' => site_url('dialogue/recycle'),
            'create_url' => site_url('dialogue/add'),
            );
        $this->data['data'] = $data;
        $this->data['page_title'] = "Manage Dialogues";
        $this->twig->display('twig/francaisfacile/list-dialogue', $this->data);
    }

    public function add() {
        $objInfo = $this->input->post('dialogue');
        $name = $objInfo['name'];
        if($name != null){
            $objInfo['user_id'] = $this->getUser()->getId();
            $result = $this->ffdialogues_model->insert($objInfo);
            if($result == true){
                $this->data['success_masage'] = "Dialogue $name has been added";
            } else{
                $this->data['error_message'] = "Dialogue $name has not been added";
            }
        }
        $categories = $this->ffcategories_model->findAllKeyAndValueOrderBy('name',true);
        $this->data['categories'] = $categories;

        $this->data['page_title'] = "Add New Dialogue";
        $this->twig->display('twig/francaisfacile/edit-dialogue', $this->data);
    }

    public function edit($id = null) {
        $objInfo = $this->input->post('dialogue');
        $name = $objInfo['name'];
        if($name != null){
            $objInfo['user_id'] = $this->getUser()->getId();
            $result = $this->ffdialogues_model->update($objInfo);
            if($result == true){
                $this->data['success_masage'] = "Dialogue $name has been updated";
            } else{
                $this->data['error_message'] = "Dialogue $name has not been updated";
            }
        }
        $obj = $this->ffdialogues_model->findById($id);
        if($obj != null){
            $this->data['dialogue'] = $obj;
        }
        $categories = $this->ffcategories_model->findAllKeyAndValueOrderBy('name',true);
        $this->data['categories'] = $categories;
        $this->data['page_title'] = "Update Dialogue";
        $this->twig->display('twig/francaisfacile/edit-dialogue', $this->data);
    }

    public function recycle(){

        $recycleid = $this->input->post('recycleid');
        if($recycleid !== null){
            $result = $this->ffdialogues_model->delete($recycleid);
            if($result == true){
                $this->data['success_masage'] = "Dialogue $recycleid has been deleted";
            } else{
                $this->data['error_message'] = "Dialogue $recycleid has not been deteted";
            }
        }

        redirect('/dialogue/index');

    }

}