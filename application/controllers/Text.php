<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 3/9/16
 * Time: 11:31 AM
 */
class Text extends  MY_Controller
{

    function __construct(){
        parent::__construct();
        $this->load->model('fftexts_model');
        $this->load->model('ffcategories_model');

    }


    public function index() {

        $objs = $this->fftexts_model->findAllTextsOrderBy('name', true);
        $this->data['texts'] = $objs;

        $data = array(
            'recycle_action' => site_url('text/recycle'),
            'create_url' => site_url('text/add'),
        );
        $this->data['data'] = $data;
        $this->data['page_title'] = "Manage Text";
        $this->twig->display('twig/francaisfacile/list-text', $this->data);
    }

    public function add() {
        $objInfo = $this->input->post('text');
        $name = $objInfo['name'];
        if($name != null){
            $objInfo['user_id'] = $this->getUser()->getId();
            $result = $this->fftexts_model->insert($objInfo);
            if($result == true){
                $this->data['success_masage'] = "Text $name has been added";
            } else{
                $this->data['error_message'] = "Text $name has not been added";
            }
        }
        $categories = $this->ffcategories_model->findAllKeyAndValueOrderBy('name',true);
        $this->data['categories'] = $categories;

        $this->data['page_title'] = "Add New Text";
        $this->twig->display('twig/francaisfacile/edit-text', $this->data);
    }

    public function edit($id = null) {
        $objInfo = $this->input->post('text');
        $name = $objInfo['name'];
        if($name != null){
            $objInfo['user_id'] = $this->getUser()->getId();
            $result = $this->fftexts_model->update($objInfo);
            if($result == true){
                $this->data['success_masage'] = "Text $name has been updated";
            } else{
                $this->data['error_message'] = "Text $name has not been updated";
            }
        }
        $obj = $this->fftexts_model->findById($id);
        if($obj != null){
            $this->data['text'] = $obj;
        }
        $categories = $this->ffcategories_model->findAllKeyAndValueOrderBy('name',true);
        $this->data['categories'] = $categories;
        $this->data['page_title'] = "Update Text";
        $this->twig->display('twig/francaisfacile/edit-text', $this->data);
    }

    public function recycle(){

        $recycleid = $this->input->post('recycleid');
        if($recycleid !== null){
            $result = $this->fftexts_model->delete($recycleid);
            if($result == true){
                $this->data['success_masage'] = "Text $recycleid has been deleted";
            } else{
                $this->data['error_message'] = "Text $recycleid has not been deteted";
            }
        }

        redirect('/text/index');

    }
}