<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 1/29/16
 * Time: 11:22 AM
 */



class User extends MY_Controller
{


    function __construct(){
        parent::__construct();
        $this->load->model('user_model');

    }

    public function index(){


//        $user = $this->em->find(1);
        $reduce_users = array();
        $users = $this->ion_auth->users()->result();
        foreach ($users as $k => $user)
        {
            $reduce_users[$k] = reduce_use_info($user);
//            $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
        }
        $this->data['users'] = $reduce_users;
        $data = array('recycle_action' => site_url('user/recycle'));
        $this->data['data'] = $data;
        $this->data['page_title'] = "Manage User";
        $this->twig->display('twig/manage/list-user', $this->data);

    }

    public function add(){
        $this->data['page_title'] = "Add User";
        $user = $this->input->post('user');
        $username = $user['username'];
        if($username != null){
            $plainpasswords = $user['plainpassword'];
            $firstname = $user['firstname'];
            $lastname = $user['lastname'];
            $emails = $user['email'];
            $phone = $user['phone'];
            $ipAddress = $this->input->ip_address();
            if($plainpasswords['first'] != $plainpasswords['second']){

            }
            if($emails['first'] != $emails['second']){

            }
            $password = $plainpasswords['first'];
            $email = $emails['first'];
            $this->load->model('user_model');
            $result = $this->user_model->insert($username,$password,$firstname,$lastname,$email,$phone,$ipAddress);
            if($result == true){
                $this->data['success_masage'] = "$username has been added";
            } else{
                $this->data['error_message'] = "$username has not been added";
            }
        }


        $this->twig->display('twig/manage/edit-user', $this->data);


    }

    /**
     * @param $userId
     */
    public function edit($userId){

        $user = $this->ion_auth->user($userId)->row();
        $userInfo = reduce_use_info($user);
        $this->data['page_title'] = "Edit User";
        $this->data['user']= $userInfo;
        $this->twig->display('twig/manage/edit-user', $this->data);

    }

    /**
     * @param $userId
     * @return boolean
     */
    public function delete($userId){

        return $this->ion_auth->delete_user($id);

    }
}