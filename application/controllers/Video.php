<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 3/10/16
 * Time: 12:53 AM
 */
class Video extends  MY_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('ffvideos_model');
        $this->load->model('ffcategories_model');

    }


    public function index() {

        $objs = $this->ffvideos_model->findAllVideosOrderBy('name', true);
        $this->data['videos'] = $objs;

        $data = array(
            'recycle_action' => site_url('video/recycle'),
            'create_url' => site_url('video/add'),
        );
        $this->data['data'] = $data;
        $this->data['page_title'] = "Manage Video";
        $this->twig->display('twig/francaisfacile/list-video', $this->data);
    }

    public function add() {
        $objInfo = $this->input->post('video');
        $name = $objInfo['name'];
        if($name != null){
            $objInfo['user_id'] = $this->getUser()->getId();
            $result = $this->ffvideos_model->insert($objInfo);
            if($result == true){
                $this->data['success_masage'] = "Video $name has been added";
            } else{
                $this->data['error_message'] = "Video $name has not been added";
            }
        }
        $categories = $this->ffcategories_model->findAllKeyAndValueOrderBy('name',true);
        $this->data['categories'] = $categories;

        $this->data['page_title'] = "Add New Video";
        $this->twig->display('twig/francaisfacile/edit-video', $this->data);
    }

    public function edit($id = null) {
        $objInfo = $this->input->post('video');
        $name = $objInfo['name'];
        if($name != null){
            $objInfo['user_id'] = $this->getUser()->getId();
            $result = $this->ffvideos_model->update($objInfo);
            if($result == true){
                $this->data['success_masage'] = "Video $name has been updated";
            } else{
                $this->data['error_message'] = "Video $name has not been updated";
            }
        }
        $obj = $this->ffvideos_model->findById($id);
        if($obj != null){
            $this->data['video'] = $obj;
        }
        $categories = $this->ffcategories_model->findAllKeyAndValueOrderBy('name',true);
        $this->data['categories'] = $categories;
        $this->data['page_title'] = "Update Video";
        $this->twig->display('twig/francaisfacile/edit-video', $this->data);
    }

    public function recycle(){

        $recycleid = $this->input->post('recycleid');
        if($recycleid !== null){
            $result = $this->ffvideos_model->delete($recycleid);
            if($result == true){
                $this->data['success_masage'] = "Video $recycleid has been deleted";
            } else{
                $this->data['error_message'] = "Video $recycleid has not been deteted";
            }
        }

        redirect('/video/index');

    }
}