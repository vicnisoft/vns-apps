<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 1/29/16
 * Time: 4:57 PM
 */
class Project extends MY_Controller
{

    function __construct(){
        parent::__construct();
        $this->load->model('projects_model');
    }

    public function index() {

        $projects = $this->projects_model->findAllProjectsOrderBy('name', true);
        $this->data['projects'] = $projects;
        $data = array('recycle_action' => site_url('project/recycle'));
        $this->data['data'] = $data;
        $this->data['page_title'] = "Manage Project";
        $this->twig->display('twig/manage/list-project', $this->data);
    }

    public function add() {
        $projectInfo = $this->input->post('project');
        $projectName = $projectInfo['name'];
        if($projectName != null){
            $projectIcon = $projectInfo['icon'];
            $projectDescription = $projectInfo['description'];
            $result = $this->projects_model->insert($projectName, $projectIcon, $projectDescription);
            if($result == true){
                $this->data['success_masage'] = "Project $projectName has been added";
            } else{
                $this->data['error_message'] = "Project $projectName has not been added";
            }
        }
        $this->data['page_title'] = "Add New Project";
        $this->twig->display('twig/manage/edit-project', $this->data);
    }

    public function edit($id = null) {
        $projectInfo = $this->input->post('project');
        $projectName = $projectInfo['name'];
        if($projectName != null){
            $projectId = $projectInfo['id'];
            $projectIcon = $projectInfo['icon'];
            $projectDescription = $projectInfo['description'];
            $result = $this->projects_model->update($projectId,$projectName, $projectIcon, $projectDescription);
            if($result == true){
                $this->data['success_masage'] = "Project $projectName has been updated";
            } else{
                $this->data['error_message'] = "Project $projectName has not been updated";
            }
        }
        $project = $this->projects_model->findById($id);
        if($project != null){
            $this->data['project'] = $project;
        }
        $this->data['page_title'] = "Update Project";
        $this->twig->display('twig/manage/edit-project', $this->data);
    }

    public function recycle(){

        $recycleid = $this->input->post('recycleid');
        if($recycleid !== null){
            $result = $this->projects_model->delete($recycleid);
            if($result == true){
                $this->data['success_masage'] = "Project $recycleid has been deleted";
            } else{
                $this->data['error_message'] = "Project $recycleid has not been deteted";
            }
        }

        redirect('/project/index');


    }
}