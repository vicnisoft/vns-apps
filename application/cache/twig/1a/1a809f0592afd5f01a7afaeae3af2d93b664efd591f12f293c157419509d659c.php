<?php

/* twig/francaisfacile/list-text.twig */
class __TwigTemplate_93abbca52264efa0518dc3459cd9030e4694017f1b8dbe96aed065076a282edc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/francaisfacile/list-text.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/buttons.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/select.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/editor.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/js/dataTables.editor.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "    <div class=\"col-md-12\">
        </br>
        <div class=\"form-inline\">
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-info\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "create_url", array()), "html", null, true);
        echo "\">Create New</a>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-default\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">Filters</a>
                </div>
            </div>
        </div>
        </br>
    </div>

    <div class=\"col-md-12\">
        <table id=\"tbl\" class=\"display\" cellspacing=\"0\" width=\"100%\">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Content</th>
                <th>Icon</th>
                <th>Category</th>
                <th width=\"18%\">Created On</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["texts"]) ? $context["texts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["text"]) {
            // line 46
            echo "                <tr>
                    <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["text"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["text"], "name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["text"], "content", array()), "html", null, true);
            echo "</td>
                    ";
            // line 51
            echo "                    <td>
                        <img src=\" ";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["text"], "icon", array()), "html", null, true);
            echo "\" class=\"img-thumbnail\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["text"], "name", array()), "html", null, true);
            echo "\" width=\"100\" height=\"auto\">
                    </td>
                    <td>";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["text"], "category", array()), "name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($context["text"], "createdOn", array()), "html", null, true);
            echo "</td>
                    <td>
                        <a title=\"Recycle\" data-recycleid=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($context["text"], "id", array()), "html", null, true);
            echo "\" data-recyclename=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["text"], "name", array()), "html", null, true);
            echo "\" data-recycleinfo=\"Text Content: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["text"], "short_content", array()), "html", null, true);
            echo "\" onclick=\"recycle(this);\" href=\"#\">
                            <i class=\"fa fa-trash fa-2x\"></i>
                        </a>
                        ";
            // line 60
            $context["edit_url"] = array(0 => "text", 1 => "edit", 2 => $this->getAttribute($context["text"], "id", array()));
            // line 61
            echo "                        <a title=\"Edit Text\" href=\"";
            echo twig_escape_filter($this->env, site_url((isset($context["edit_url"]) ? $context["edit_url"] : null)), "html", null, true);
            echo "\">
                            <i class=\"fa fa-pencil-square-o fa-2x\"></i>
                        </a>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['text'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "            </tbody>
        </table>
        ";
        // line 69
        echo twig_include($this->env, $context, "twig/manage/inc/recycle.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    </div>
    ";
    }

    // line 75
    public function block_javascripts($context, array $blocks = array())
    {
        // line 76
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 77
        echo twig_include($this->env, $context, "twig/manage/inc/recycle-js.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    <script>
        \$(document).ready(function () {
            \$('#tbl').DataTable();
        });
        \$('#tbl')
                .removeClass( 'display' )
                .addClass('table table-striped table-bordered');
    </script>
";
    }

    public function getTemplateName()
    {
        return "twig/francaisfacile/list-text.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 77,  179 => 76,  176 => 75,  168 => 69,  164 => 67,  151 => 61,  149 => 60,  139 => 57,  134 => 55,  130 => 54,  123 => 52,  120 => 51,  116 => 49,  112 => 48,  108 => 47,  105 => 46,  101 => 45,  72 => 19,  65 => 14,  62 => 13,  56 => 10,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/*     <link href="{{ base_url("public/vendor/datatables/css/jquery.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/buttons.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/select.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/editor.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <script src="{{ base_url("public/vendor/datatables/js/dataTables.editor.min.js") }}"></script>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="col-md-12">*/
/*         </br>*/
/*         <div class="form-inline">*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-info" href="{{ data.create_url }}">Create New</a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-default" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Filters</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         </br>*/
/*     </div>*/
/* */
/*     <div class="col-md-12">*/
/*         <table id="tbl" class="display" cellspacing="0" width="100%">*/
/*             <thead>*/
/*             <tr>*/
/*                 <th>Id</th>*/
/*                 <th>Name</th>*/
/*                 <th>Content</th>*/
/*                 <th>Icon</th>*/
/*                 <th>Category</th>*/
/*                 <th width="18%">Created On</th>*/
/*                 <th>Actions</th>*/
/*             </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             {% for text in texts %}*/
/*                 <tr>*/
/*                     <td>{{ text.id }}</td>*/
/*                     <td>{{ text.name }}</td>*/
/*                     <td>{{ text.content }}</td>*/
/*                     {#<td>{{ text.icon }}</td>#}*/
/*                     <td>*/
/*                         <img src=" {{ text.icon }}" class="img-thumbnail" alt="{{ text.name }}" width="100" height="auto">*/
/*                     </td>*/
/*                     <td>{{ text.category.name }}</td>*/
/*                     <td>{{ text.createdOn }}</td>*/
/*                     <td>*/
/*                         <a title="Recycle" data-recycleid="{{ text.id }}" data-recyclename="{{ text.name }}" data-recycleinfo="Text Content: {{ text.short_content }}" onclick="recycle(this);" href="#">*/
/*                             <i class="fa fa-trash fa-2x"></i>*/
/*                         </a>*/
/*                         {% set edit_url = ['text','edit',text.id] %}*/
/*                         <a title="Edit Text" href="{{ site_url(edit_url)}}">*/
/*                             <i class="fa fa-pencil-square-o fa-2x"></i>*/
/*                         </a>*/
/*                     </td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*         {{ include('twig/manage/inc/recycle.twig', { 'data': data }) }}*/
/* */
/*     </div>*/
/*     {#{{ dump(texts) }}#}*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     {{ include("twig/manage/inc/recycle-js.twig", {'data':data}) }}*/
/* */
/*     <script>*/
/*         $(document).ready(function () {*/
/*             $('#tbl').DataTable();*/
/*         });*/
/*         $('#tbl')*/
/*                 .removeClass( 'display' )*/
/*                 .addClass('table table-striped table-bordered');*/
/*     </script>*/
/* {% endblock %}*/
/* */
