<?php

/* twig/base.twig */
class __TwigTemplate_728413ec5e66ea23b8d013041f6359554f24529b6a6c16d038f7d3668d82fd9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'page_title' => array($this, 'block_page_title'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
    <meta charset=\"utf-8\" />
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />

    ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 21
        echo "
    <title>";
        // line 22
        $this->displayBlock('title', $context, $blocks);
        echo "</title>


</head>
<body>
<div id=\"wrapper\">
    ";
        // line 28
        if ((isset($context["user"]) ? $context["user"] : null)) {
            // line 29
            echo "    ";
            echo twig_include($this->env, $context, "twig/layout/navigation.twig");
            echo "
    ";
        }
        // line 31
        echo "    <!-- /. NAV TOP  -->
    ";
        // line 32
        if ((isset($context["user"]) ? $context["user"] : null)) {
            // line 33
            echo "        ";
            echo twig_include($this->env, $context, "twig/layout/navigation_side.twig");
            echo "
        <!-- /. NAV SIDE  -->
    ";
        }
        // line 36
        echo "    <div class=\"col-lg-12\" id=\"page-header\">
        <h1>
            ";
        // line 38
        $this->displayBlock('page_title', $context, $blocks);
        // line 40
        echo "        </h1>
        </hr>
    </div>
    <div ";
        // line 43
        if ((isset($context["user"]) ? $context["user"] : null)) {
            echo " id=\"page-wrapper\" ";
        } else {
            echo " id=\"container\"";
        }
        echo ">

        ";
        // line 45
        $this->displayBlock('content', $context, $blocks);
        // line 46
        echo "    </div>
    <!-- /. PAGE WRAPPER  -->

</div>
<!-- /. WRAPPER  -->
<!-- /. FOOTER  -->
";
        // line 52
        echo twig_include($this->env, $context, "twig/layout/footer.twig");
        echo "

";
        // line 54
        $this->displayBlock('javascripts', $context, $blocks);
        // line 74
        echo "

</body>
</html>
";
    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 8
        echo "        <!-- BOOTSTRAP STYLES-->
        <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <!-- BOOTSTRAP DATATABLEs STYLES-->
        <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/dt-1.10.8_datatables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <!-- FONTAWESOME STYLES-->
        <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <!--CUSTOM BASIC STYLES-->
        <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/css/basic.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <!--CUSTOM MAIN STYLES-->
        <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <!-- GOOGLE FONTS-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    ";
    }

    // line 22
    public function block_title($context, array $blocks = array())
    {
        echo "Vicnisoft";
        if (array_key_exists("title", $context)) {
            echo " | ";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        }
        if (array_key_exists("subtitle", $context)) {
            echo " &gt; ";
            echo twig_escape_filter($this->env, (isset($context["subtitle"]) ? $context["subtitle"] : null), "html", null, true);
        }
    }

    // line 38
    public function block_page_title($context, array $blocks = array())
    {
        if (array_key_exists("page_title", $context)) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["page_title"]) ? $context["page_title"] : null), "html", null, true);
        }
        if (array_key_exists("sub_page_title", $context)) {
            echo " &gt; ";
            echo twig_escape_filter($this->env, (isset($context["sub_page_title"]) ? $context["sub_page_title"] : null), "html", null, true);
            echo "
            ";
        }
    }

    // line 45
    public function block_content($context, array $blocks = array())
    {
    }

    // line 54
    public function block_javascripts($context, array $blocks = array())
    {
        // line 55
        echo "    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src=\"";
        // line 57
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/js/jquery-1.10.2.js"), "html", null, true);
        echo "\"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src=\"";
        // line 59
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <!-- METISMENU SCRIPTS -->
    <script src=\"";
        // line 61
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/js/jquery.metisMenu.js"), "html", null, true);
        echo "\"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src=\"";
        // line 63
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/js/custom.js"), "html", null, true);
        echo "\"></script>
    <!-- DATATABLEs SCRIPTS -->
    <script src=\"";
        // line 65
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/js/datatables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 66
        echo twig_escape_filter($this->env, base_url("public/js/main.js"), "html", null, true);
        echo "\"></script>

    <script>
        \$(document).ready(function () {
            showLastLogin(";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "last_login", array()), "html", null, true);
        echo ")
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "twig/base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 70,  210 => 66,  206 => 65,  201 => 63,  196 => 61,  191 => 59,  186 => 57,  182 => 55,  179 => 54,  174 => 45,  159 => 38,  145 => 22,  137 => 17,  132 => 15,  127 => 13,  122 => 11,  117 => 9,  114 => 8,  111 => 7,  103 => 74,  101 => 54,  96 => 52,  88 => 46,  86 => 45,  77 => 43,  72 => 40,  70 => 38,  66 => 36,  59 => 33,  57 => 32,  54 => 31,  48 => 29,  46 => 28,  37 => 22,  34 => 21,  32 => 7,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html xmlns="http://www.w3.org/1999/xhtml">*/
/* <head>*/
/*     <meta charset="utf-8" />*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0" />*/
/* */
/*     {% block stylesheets %}*/
/*         <!-- BOOTSTRAP STYLES-->*/
/*         <link href="{{ base_url("public/vendor/bs-advance-admin/css/bootstrap.css") }}" rel="stylesheet" />*/
/*         <!-- BOOTSTRAP DATATABLEs STYLES-->*/
/*         <link href="{{ base_url("public/vendor/datatables/css/dt-1.10.8_datatables.min.css") }}" rel="stylesheet" />*/
/*         <!-- FONTAWESOME STYLES-->*/
/*         <link href="{{ base_url("public/vendor/bs-advance-admin/css/font-awesome.css") }}" rel="stylesheet" />*/
/*         <!--CUSTOM BASIC STYLES-->*/
/*         <link href="{{ base_url("public/vendor/bs-advance-admin/css/basic.css") }}" rel="stylesheet" />*/
/*         <!--CUSTOM MAIN STYLES-->*/
/*         <link href="{{ base_url("public/vendor/bs-advance-admin/css/custom.css") }}" rel="stylesheet" />*/
/*         <!-- GOOGLE FONTS-->*/
/*         <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />*/
/*     {% endblock %}*/
/* */
/*     <title>{% block title %}Vicnisoft{% if title is defined %} | {{ title }}{% endif %}{% if subtitle is defined %} &gt; {{ subtitle }}{% endif %}{% endblock %}</title>*/
/* */
/* */
/* </head>*/
/* <body>*/
/* <div id="wrapper">*/
/*     {% if user %}*/
/*     {{ include('twig/layout/navigation.twig') }}*/
/*     {% endif %}*/
/*     <!-- /. NAV TOP  -->*/
/*     {% if user %}*/
/*         {{ include('twig/layout/navigation_side.twig') }}*/
/*         <!-- /. NAV SIDE  -->*/
/*     {% endif %}*/
/*     <div class="col-lg-12" id="page-header">*/
/*         <h1>*/
/*             {% block page_title %}{% if page_title is defined %} {{ page_title }}{% endif %}{% if sub_page_title is defined %} &gt; {{ sub_page_title }}*/
/*             {% endif %}{% endblock %}*/
/*         </h1>*/
/*         </hr>*/
/*     </div>*/
/*     <div {% if user %} id="page-wrapper" {% else %} id="container"{% endif %}>*/
/* */
/*         {% block content %}{% endblock %}*/
/*     </div>*/
/*     <!-- /. PAGE WRAPPER  -->*/
/* */
/* </div>*/
/* <!-- /. WRAPPER  -->*/
/* <!-- /. FOOTER  -->*/
/* {{ include('twig/layout/footer.twig') }}*/
/* */
/* {% block javascripts %}*/
/*     <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->*/
/*     <!-- JQUERY SCRIPTS -->*/
/*     <script src="{{ base_url("public/vendor/bs-advance-admin/js/jquery-1.10.2.js") }}"></script>*/
/*     <!-- BOOTSTRAP SCRIPTS -->*/
/*     <script src="{{ base_url("public/vendor/bs-advance-admin/js/bootstrap.js") }}"></script>*/
/*     <!-- METISMENU SCRIPTS -->*/
/*     <script src="{{ base_url("public/vendor/bs-advance-admin/js/jquery.metisMenu.js") }}"></script>*/
/*     <!-- CUSTOM SCRIPTS -->*/
/*     <script src="{{ base_url("public/vendor/bs-advance-admin/js/custom.js") }}"></script>*/
/*     <!-- DATATABLEs SCRIPTS -->*/
/*     <script src="{{ base_url("public/vendor/datatables/js/datatables.min.js") }}"></script>*/
/*     <script src="{{ base_url("public/js/main.js") }}"></script>*/
/* */
/*     <script>*/
/*         $(document).ready(function () {*/
/*             showLastLogin({{ user.last_login }})*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
/* */
/* */
/* </body>*/
/* </html>*/
/* */
