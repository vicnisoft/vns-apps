<?php

/* twig/francaisfacile/edit-text.twig */
class __TwigTemplate_152ed03b1c4c9d6f66226e9c812765afba00cb7605b742ced038aa4ad3d35637 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/francaisfacile/edit-text.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div>
        <form name=\"text\" method=\"post\" action=\"\"  novalidate=\"novalidate\">
            <div class=\"";
        // line 12
        if ( !array_key_exists("error_message", $context)) {
            echo "hide";
        }
        echo " alert alert-danger\" id=\"edit-project-errors\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Error!</strong>
                <span class=\"message\">";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null), "html", null, true);
        echo "</span>
            </div>
            <div class=\"";
        // line 17
        if ( !array_key_exists("success_masage", $context)) {
            echo "hide";
        }
        echo " alert alert-success\" id=\"edit-project-errors\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Success!</strong>
                <span class=\"message\">";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["success_masage"]) ? $context["success_masage"] : null), "html", null, true);
        echo "</span>
            </div>
            <fieldset>
                <div class=\"row\">
                    ";
        // line 24
        if ( !array_key_exists("text", $context)) {
            // line 25
            echo "                        <div class=\"col-md-12 \">
                            <div class=\"panel panel-primary\">
                                <div class=\"panel-heading\">
                                    Create new text
                                </div>
                                <div class=\"panel-body\">
                                    <div class=\"col-md-12\">
                                        <div class=\"form-group\">
                                            <label for=\"text_name\" class=\"control-label  required\">
                                                Name *
                                            </label>
                                            <input id=\"text_name\" name=\"text[name]\" required=\"required\" class=\"form-control\" type=\"text\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"text_audio\" class=\"control-label  required\">
                                                Audio *
                                            </label>
                                            <input id=\"text_audio\" name=\"text[audio_url]\" required=\"required\" class=\"form-control\" type=\"url\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"text_icon\" class=\"control-label  required\">
                                                Icon *
                                            </label>
                                            <input id=\"text_icon\" name=\"text[icon]\" required=\"required\" class=\"form-control\" type=\"url\">
                                        </div>
                                    </div>
                                    <div class=\"col-md-12\">
                                        <div class=\"form-group\">
                                            <label for=\"text_category\" class=\"control-label  required\">
                                                Category *
                                            </label>
                                            <select id=\"text_category\" name=\"text[category_id]\" class=\"form-control\">
                                                ";
            // line 57
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 58
                echo "                                                    <option  selected=\"false\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "value", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "key", array()), "html", null, true);
                echo "</option>
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 60
            echo "                                            </select>
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"text_content\" class=\"control-label  required\">
                                                Content *
                                            </label>
                                            <textarea  id=\"text_content\" style=\"height: 208px; overflow-y: hidden;\" rows=\"3\" name=\"text[content]\" required=\"required\" class=\" form-control\">
                                            </textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    ";
        } else {
            // line 75
            echo "                        <div class=\"col-md-12\">
                            <div class=\"panel panel-primary\">
                                <div class=\"panel-heading\">
                                    Update text
                                </div>
                                <div class=\"panel-body\">
                                    <div class=\"col-md-12\">
                                        <div class=\"form-group\">
                                            <input id=\"text_id\" name=\"text[text_id]\" required=\"required\" class=\"form-control\" type=\"hidden\" value=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["text"]) ? $context["text"] : null), "id", array()), "html", null, true);
            echo "\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"text_name\" class=\"control-label  required\">
                                                Name *
                                            </label>
                                            <input id=\"text_name\" name=\"text[name]\" required=\"required\" class=\"form-control\" type=\"text\" value=\"";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["text"]) ? $context["text"] : null), "name", array()), "html", null, true);
            echo "\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"text_audio\" class=\"control-label  required\">
                                                Audio *
                                            </label>
                                            <input id=\"text_audio\" name=\"text[audio_url]\" required=\"required\" class=\"form-control\" type=\"url\" value=\"";
            // line 95
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["text"]) ? $context["text"] : null), "audioUrl", array()), "html", null, true);
            echo "\" onchange=\"updateAudio()\">
                                        </div>
                                        <div >
                                            <audio controls>
                                                <source id=\"audio_source\" src=\"";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["text"]) ? $context["text"] : null), "audioUrl", array()), "html", null, true);
            echo "\" type=\"audio/mpeg\" />
                                            </audio>
                                        </div>
                                    </div>
                                    <div class=\"col-md-12\">
                                        <div class=\"form-group\">
                                            <input name=\"text[old_category]\" class=\"form-control\" type=\"hidden\" value=\"";
            // line 105
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["text"]) ? $context["text"] : null), "category", array()), "id", array()), "html", null, true);
            echo "\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"text_icon\" class=\"control-label  required\">
                                                Icon *
                                            </label>
                                            <input id=\"text_icon\" name=\"text[icon]\" required=\"required\" class=\"form-control\" type=\"url\" value=\"";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["text"]) ? $context["text"] : null), "iconUrl", array()), "html", null, true);
            echo "\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"text_category\" class=\"control-label  required\">
                                                Category *
                                            </label>
                                            <select class=\"form-control\" id=\"text_category\" name=\"text[category_id]\">
                                                ";
            // line 118
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 119
                echo "                                                    ";
                if (($this->getAttribute($this->getAttribute((isset($context["text"]) ? $context["text"] : null), "category", array()), "id", array()) == $this->getAttribute($context["category"], "value", array()))) {
                    // line 120
                    echo "                                                        <option   value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "value", array()), "html", null, true);
                    echo "\" selected>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "key", array()), "html", null, true);
                    echo "</option>
                                                    ";
                } else {
                    // line 122
                    echo "                                                        <option  value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "value", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "key", array()), "html", null, true);
                    echo "</option>
                                                    ";
                }
                // line 124
                echo "                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 125
            echo "                                            </select>
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"text_content\" class=\"control-label  required\">
                                                Content *
                                            </label>
                                            <textarea  id=\"text_content\"  rows=\"10\" name=\"text[content]\" required=\"required\" class=\" form-control\">
                                                ";
            // line 132
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["text"]) ? $context["text"] : null), "content", array()), "html", null, true);
            echo "
                                            </textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    ";
        }
        // line 141
        echo "            </fieldset>
            <hr>
            <div class=\"form_buttons pull-right\">
                <button type=\"submit\"  class=\"btn btn-primary\">
                    Save
                </button>
                <a  href=\"";
        // line 147
        echo twig_escape_filter($this->env, site_url("text/index"), "html", null, true);
        echo "\" class=\"btn btn-default\">
                    Cancel
                </a>
            </div>
            <div class=\"clear\"></div>
            <input id=\"user__token\" name=\"user[_token]\" class=\"form-control\" value=\"v6JhOQJ_IZ8RBG1SClYzx8AdxWCdqQ4WjKWylUalX4g\" type=\"hidden\"></form>
        <div class=\"row\">
            <div class=\"col-md-6\"></div>
        </div>
    </div>
";
    }

    // line 159
    public function block_javascripts($context, array $blocks = array())
    {
        // line 160
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 161
        echo twig_escape_filter($this->env, base_url("public/vendor/bootstrap3_player.js"), "html", null, true);
        echo "\"></script>

";
    }

    public function getTemplateName()
    {
        return "twig/francaisfacile/edit-text.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  290 => 161,  285 => 160,  282 => 159,  267 => 147,  259 => 141,  247 => 132,  238 => 125,  232 => 124,  224 => 122,  216 => 120,  213 => 119,  209 => 118,  199 => 111,  190 => 105,  181 => 99,  174 => 95,  165 => 89,  156 => 83,  146 => 75,  129 => 60,  118 => 58,  114 => 57,  80 => 25,  78 => 24,  71 => 20,  63 => 17,  58 => 15,  50 => 12,  46 => 10,  43 => 9,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div>*/
/*         <form name="text" method="post" action=""  novalidate="novalidate">*/
/*             <div class="{% if (error_message is not defined ) %}hide{% endif %} alert alert-danger" id="edit-project-errors">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Error!</strong>*/
/*                 <span class="message">{{ error_message }}</span>*/
/*             </div>*/
/*             <div class="{% if (success_masage is not defined ) %}hide{% endif %} alert alert-success" id="edit-project-errors">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Success!</strong>*/
/*                 <span class="message">{{ success_masage }}</span>*/
/*             </div>*/
/*             <fieldset>*/
/*                 <div class="row">*/
/*                     {% if text is not defined %}*/
/*                         <div class="col-md-12 ">*/
/*                             <div class="panel panel-primary">*/
/*                                 <div class="panel-heading">*/
/*                                     Create new text*/
/*                                 </div>*/
/*                                 <div class="panel-body">*/
/*                                     <div class="col-md-12">*/
/*                                         <div class="form-group">*/
/*                                             <label for="text_name" class="control-label  required">*/
/*                                                 Name **/
/*                                             </label>*/
/*                                             <input id="text_name" name="text[name]" required="required" class="form-control" type="text">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="text_audio" class="control-label  required">*/
/*                                                 Audio **/
/*                                             </label>*/
/*                                             <input id="text_audio" name="text[audio_url]" required="required" class="form-control" type="url">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="text_icon" class="control-label  required">*/
/*                                                 Icon **/
/*                                             </label>*/
/*                                             <input id="text_icon" name="text[icon]" required="required" class="form-control" type="url">*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     <div class="col-md-12">*/
/*                                         <div class="form-group">*/
/*                                             <label for="text_category" class="control-label  required">*/
/*                                                 Category **/
/*                                             </label>*/
/*                                             <select id="text_category" name="text[category_id]" class="form-control">*/
/*                                                 {% for category in categories %}*/
/*                                                     <option  selected="false" value="{{ category.value }}">{{ category.key }}</option>*/
/*                                                 {% endfor %}*/
/*                                             </select>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="text_content" class="control-label  required">*/
/*                                                 Content **/
/*                                             </label>*/
/*                                             <textarea  id="text_content" style="height: 208px; overflow-y: hidden;" rows="3" name="text[content]" required="required" class=" form-control">*/
/*                                             </textarea>*/
/*                                         </div>*/
/* */
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% else %}*/
/*                         <div class="col-md-12">*/
/*                             <div class="panel panel-primary">*/
/*                                 <div class="panel-heading">*/
/*                                     Update text*/
/*                                 </div>*/
/*                                 <div class="panel-body">*/
/*                                     <div class="col-md-12">*/
/*                                         <div class="form-group">*/
/*                                             <input id="text_id" name="text[text_id]" required="required" class="form-control" type="hidden" value="{{ text.id }}">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="text_name" class="control-label  required">*/
/*                                                 Name **/
/*                                             </label>*/
/*                                             <input id="text_name" name="text[name]" required="required" class="form-control" type="text" value="{{ text.name }}">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="text_audio" class="control-label  required">*/
/*                                                 Audio **/
/*                                             </label>*/
/*                                             <input id="text_audio" name="text[audio_url]" required="required" class="form-control" type="url" value="{{ text.audioUrl }}" onchange="updateAudio()">*/
/*                                         </div>*/
/*                                         <div >*/
/*                                             <audio controls>*/
/*                                                 <source id="audio_source" src="{{ text.audioUrl }}" type="audio/mpeg" />*/
/*                                             </audio>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     <div class="col-md-12">*/
/*                                         <div class="form-group">*/
/*                                             <input name="text[old_category]" class="form-control" type="hidden" value="{{ text.category.id }}">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="text_icon" class="control-label  required">*/
/*                                                 Icon **/
/*                                             </label>*/
/*                                             <input id="text_icon" name="text[icon]" required="required" class="form-control" type="url" value="{{ text.iconUrl }}">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="text_category" class="control-label  required">*/
/*                                                 Category **/
/*                                             </label>*/
/*                                             <select class="form-control" id="text_category" name="text[category_id]">*/
/*                                                 {% for category in categories %}*/
/*                                                     {% if text.category.id == category.value %}*/
/*                                                         <option   value="{{ category.value }}" selected>{{ category.key }}</option>*/
/*                                                     {% else %}*/
/*                                                         <option  value="{{ category.value }}">{{ category.key }}</option>*/
/*                                                     {% endif %}*/
/*                                                 {% endfor %}*/
/*                                             </select>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="text_content" class="control-label  required">*/
/*                                                 Content **/
/*                                             </label>*/
/*                                             <textarea  id="text_content"  rows="10" name="text[content]" required="required" class=" form-control">*/
/*                                                 {{ text.content }}*/
/*                                             </textarea>*/
/*                                         </div>*/
/* */
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% endif %}*/
/*             </fieldset>*/
/*             <hr>*/
/*             <div class="form_buttons pull-right">*/
/*                 <button type="submit"  class="btn btn-primary">*/
/*                     Save*/
/*                 </button>*/
/*                 <a  href="{{ site_url('text/index') }}" class="btn btn-default">*/
/*                     Cancel*/
/*                 </a>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/*             <input id="user__token" name="user[_token]" class="form-control" value="v6JhOQJ_IZ8RBG1SClYzx8AdxWCdqQ4WjKWylUalX4g" type="hidden"></form>*/
/*         <div class="row">*/
/*             <div class="col-md-6"></div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script src="{{ base_url("public/vendor/bootstrap3_player.js") }}"></script>*/
/* */
/* {% endblock %}*/
/* */
