<?php

/* twig/base.twig */
class __TwigTemplate_6eefbd63b500aa85e6b5a3f59e9e0e8fec7ebe8c2efe4526a56be9114dafee4d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'page_title' => array($this, 'block_page_title'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
    <meta charset=\"utf-8\" />
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />

    ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 21
        echo "
    <title>";
        // line 22
        $this->displayBlock('title', $context, $blocks);
        echo "</title>


</head>
<body>
<!-- Transparent overlay -->
<div class=\"overlay hide\"></div>

<div id=\"wrapper\">
    ";
        // line 31
        if ((isset($context["user"]) ? $context["user"] : null)) {
            // line 32
            echo "    ";
            echo twig_include($this->env, $context, "twig/layout/navigation.twig");
            echo "
    ";
        }
        // line 34
        echo "    <!-- /. NAV TOP  -->
    ";
        // line 35
        if ((isset($context["user"]) ? $context["user"] : null)) {
            // line 36
            echo "        ";
            echo twig_include($this->env, $context, "twig/layout/navigation_side.twig");
            echo "
        <!-- /. NAV SIDE  -->
    ";
        }
        // line 39
        echo "    <div class=\"col-lg-12\" id=\"page-header\">
        <h1>
            ";
        // line 41
        $this->displayBlock('page_title', $context, $blocks);
        // line 43
        echo "        </h1>
        </hr>
    </div>
    <div ";
        // line 46
        if ((isset($context["user"]) ? $context["user"] : null)) {
            echo " id=\"page-wrapper\" ";
        } else {
            echo " id=\"container\"";
        }
        echo ">

        ";
        // line 48
        $this->displayBlock('content', $context, $blocks);
        // line 49
        echo "    </div>
    <!-- /. PAGE WRAPPER  -->

</div>
<!-- /. WRAPPER  -->
<!-- /. FOOTER  -->
";
        // line 55
        echo twig_include($this->env, $context, "twig/layout/footer.twig");
        echo "

";
        // line 57
        $this->displayBlock('javascripts', $context, $blocks);
        // line 77
        echo "

</body>
</html>
";
    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 8
        echo "        <!-- BOOTSTRAP STYLES-->
        <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <!-- BOOTSTRAP DATATABLEs STYLES-->
        <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/dt-1.10.8_datatables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <!-- FONTAWESOME STYLES-->
        <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <!--CUSTOM BASIC STYLES-->
        <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/css/basic.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <!--CUSTOM MAIN STYLES-->
        <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <!-- GOOGLE FONTS-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    ";
    }

    // line 22
    public function block_title($context, array $blocks = array())
    {
        echo "Vicnisoft";
        if (array_key_exists("title", $context)) {
            echo " | ";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        }
        if (array_key_exists("subtitle", $context)) {
            echo " &gt; ";
            echo twig_escape_filter($this->env, (isset($context["subtitle"]) ? $context["subtitle"] : null), "html", null, true);
        }
    }

    // line 41
    public function block_page_title($context, array $blocks = array())
    {
        if (array_key_exists("page_title", $context)) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["page_title"]) ? $context["page_title"] : null), "html", null, true);
        }
        if (array_key_exists("sub_page_title", $context)) {
            echo " &gt; ";
            echo twig_escape_filter($this->env, (isset($context["sub_page_title"]) ? $context["sub_page_title"] : null), "html", null, true);
            echo "
            ";
        }
    }

    // line 48
    public function block_content($context, array $blocks = array())
    {
    }

    // line 57
    public function block_javascripts($context, array $blocks = array())
    {
        // line 58
        echo "    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src=\"";
        // line 60
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/js/jquery-1.10.2.js"), "html", null, true);
        echo "\"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src=\"";
        // line 62
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <!-- METISMENU SCRIPTS -->
    <script src=\"";
        // line 64
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/js/jquery.metisMenu.js"), "html", null, true);
        echo "\"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src=\"";
        // line 66
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/js/custom.js"), "html", null, true);
        echo "\"></script>
    <!-- DATATABLEs SCRIPTS -->
    <script src=\"";
        // line 68
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/js/datatables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 69
        echo twig_escape_filter($this->env, base_url("public/js/main.js"), "html", null, true);
        echo "\"></script>

    <script>
        \$(document).ready(function () {
            showLastLogin(";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "last_login", array()), "html", null, true);
        echo ")
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "twig/base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  220 => 73,  213 => 69,  209 => 68,  204 => 66,  199 => 64,  194 => 62,  189 => 60,  185 => 58,  182 => 57,  177 => 48,  162 => 41,  148 => 22,  140 => 17,  135 => 15,  130 => 13,  125 => 11,  120 => 9,  117 => 8,  114 => 7,  106 => 77,  104 => 57,  99 => 55,  91 => 49,  89 => 48,  80 => 46,  75 => 43,  73 => 41,  69 => 39,  62 => 36,  60 => 35,  57 => 34,  51 => 32,  49 => 31,  37 => 22,  34 => 21,  32 => 7,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html xmlns="http://www.w3.org/1999/xhtml">*/
/* <head>*/
/*     <meta charset="utf-8" />*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0" />*/
/* */
/*     {% block stylesheets %}*/
/*         <!-- BOOTSTRAP STYLES-->*/
/*         <link href="{{ base_url("public/vendor/bs-advance-admin/css/bootstrap.css") }}" rel="stylesheet" />*/
/*         <!-- BOOTSTRAP DATATABLEs STYLES-->*/
/*         <link href="{{ base_url("public/vendor/datatables/css/dt-1.10.8_datatables.min.css") }}" rel="stylesheet" />*/
/*         <!-- FONTAWESOME STYLES-->*/
/*         <link href="{{ base_url("public/vendor/bs-advance-admin/css/font-awesome.css") }}" rel="stylesheet" />*/
/*         <!--CUSTOM BASIC STYLES-->*/
/*         <link href="{{ base_url("public/vendor/bs-advance-admin/css/basic.css") }}" rel="stylesheet" />*/
/*         <!--CUSTOM MAIN STYLES-->*/
/*         <link href="{{ base_url("public/vendor/bs-advance-admin/css/custom.css") }}" rel="stylesheet" />*/
/*         <!-- GOOGLE FONTS-->*/
/*         <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />*/
/*     {% endblock %}*/
/* */
/*     <title>{% block title %}Vicnisoft{% if title is defined %} | {{ title }}{% endif %}{% if subtitle is defined %} &gt; {{ subtitle }}{% endif %}{% endblock %}</title>*/
/* */
/* */
/* </head>*/
/* <body>*/
/* <!-- Transparent overlay -->*/
/* <div class="overlay hide"></div>*/
/* */
/* <div id="wrapper">*/
/*     {% if user %}*/
/*     {{ include('twig/layout/navigation.twig') }}*/
/*     {% endif %}*/
/*     <!-- /. NAV TOP  -->*/
/*     {% if user %}*/
/*         {{ include('twig/layout/navigation_side.twig') }}*/
/*         <!-- /. NAV SIDE  -->*/
/*     {% endif %}*/
/*     <div class="col-lg-12" id="page-header">*/
/*         <h1>*/
/*             {% block page_title %}{% if page_title is defined %} {{ page_title }}{% endif %}{% if sub_page_title is defined %} &gt; {{ sub_page_title }}*/
/*             {% endif %}{% endblock %}*/
/*         </h1>*/
/*         </hr>*/
/*     </div>*/
/*     <div {% if user %} id="page-wrapper" {% else %} id="container"{% endif %}>*/
/* */
/*         {% block content %}{% endblock %}*/
/*     </div>*/
/*     <!-- /. PAGE WRAPPER  -->*/
/* */
/* </div>*/
/* <!-- /. WRAPPER  -->*/
/* <!-- /. FOOTER  -->*/
/* {{ include('twig/layout/footer.twig') }}*/
/* */
/* {% block javascripts %}*/
/*     <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->*/
/*     <!-- JQUERY SCRIPTS -->*/
/*     <script src="{{ base_url("public/vendor/bs-advance-admin/js/jquery-1.10.2.js") }}"></script>*/
/*     <!-- BOOTSTRAP SCRIPTS -->*/
/*     <script src="{{ base_url("public/vendor/bs-advance-admin/js/bootstrap.js") }}"></script>*/
/*     <!-- METISMENU SCRIPTS -->*/
/*     <script src="{{ base_url("public/vendor/bs-advance-admin/js/jquery.metisMenu.js") }}"></script>*/
/*     <!-- CUSTOM SCRIPTS -->*/
/*     <script src="{{ base_url("public/vendor/bs-advance-admin/js/custom.js") }}"></script>*/
/*     <!-- DATATABLEs SCRIPTS -->*/
/*     <script src="{{ base_url("public/vendor/datatables/js/datatables.min.js") }}"></script>*/
/*     <script src="{{ base_url("public/js/main.js") }}"></script>*/
/* */
/*     <script>*/
/*         $(document).ready(function () {*/
/*             showLastLogin({{ user.last_login }})*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
/* */
/* */
/* </body>*/
/* </html>*/
/* */
