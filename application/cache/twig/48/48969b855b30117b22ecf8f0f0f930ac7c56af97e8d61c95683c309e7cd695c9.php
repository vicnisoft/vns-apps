<?php

/* twig/manage/list-project.twig */
class __TwigTemplate_8bddc7f9ae800da8be1cffea13d87639ce0707207bdc7e8ef23af269ac51e761 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/manage/list-project.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/buttons.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/select.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/editor.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/js/dataTables.editor.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "    <div class=\"col-md-12\">
        </br>
        <div class=\"form-inline\">
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-info\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, site_url("project/add"), "html", null, true);
        echo "\">Create New</a>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-default\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">Filters</a>
                </div>
            </div>
        </div>
        </br>
    </div>

    <div class=\"col-md-12\">
        <table id=\"tbl\" class=\"display\" cellspacing=\"0\" width=\"100%\">
            <thead>
            <tr>
                ";
        // line 36
        echo "                <th>Id</th>
                <th>Project Name</th>
                <th>Description</th>
                <th>Project Icon</th>
                <th width=\"18%\">Created date</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["projects"]) ? $context["projects"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["project"]) {
            // line 46
            echo "                <tr>
                    ";
            // line 48
            echo "                    <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["project"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["project"], "name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["project"], "short_desctiption", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["project"], "project_icon", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["project"], "created_on", array()), "html", null, true);
            echo "</td>
                    <td>
                        <a title=\"Recycle\" data-recycleid=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["project"], "id", array()), "html", null, true);
            echo "\" data-recyclename=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["project"], "name", array()), "html", null, true);
            echo "\" data-recycleinfo=\"Project Description: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["project"], "desc", array()), "html", null, true);
            echo "\" onclick=\"recycle(this);\" href=\"#\">
                            <i class=\"fa fa-trash fa-2x\"></i>
                        </a>
                        ";
            // line 57
            $context["edit_url"] = array(0 => "project", 1 => "edit", 2 => $this->getAttribute($context["project"], "id", array()));
            // line 58
            echo "                        <a title=\"Edit Project\" href=\"";
            echo twig_escape_filter($this->env, site_url((isset($context["edit_url"]) ? $context["edit_url"] : null)), "html", null, true);
            echo "\">
                            <i class=\"fa fa-pencil-square-o fa-2x\"></i>
                        </a>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['project'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "            </tbody>
        </table>
        ";
        // line 66
        echo twig_include($this->env, $context, "twig/manage/inc/recycle.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    </div>
";
    }

    // line 71
    public function block_javascripts($context, array $blocks = array())
    {
        // line 72
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 73
        echo twig_include($this->env, $context, "twig/manage/inc/recycle-js.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    <script>
        \$(document).ready(function () {
            \$('#tbl').DataTable();
        });
        \$('#tbl')
                .removeClass( 'display' )
                .addClass('table table-striped table-bordered');

    </script>
";
    }

    public function getTemplateName()
    {
        return "twig/manage/list-project.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 73,  171 => 72,  168 => 71,  160 => 66,  156 => 64,  143 => 58,  141 => 57,  131 => 54,  126 => 52,  122 => 51,  118 => 50,  114 => 49,  109 => 48,  106 => 46,  102 => 45,  91 => 36,  72 => 19,  65 => 14,  62 => 13,  56 => 10,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/*     <link href="{{ base_url("public/vendor/datatables/css/jquery.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/buttons.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/select.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/editor.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <script src="{{ base_url("public/vendor/datatables/js/dataTables.editor.min.js") }}"></script>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="col-md-12">*/
/*         </br>*/
/*         <div class="form-inline">*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-info" href="{{ site_url('project/add') }}">Create New</a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-default" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Filters</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         </br>*/
/*     </div>*/
/* */
/*     <div class="col-md-12">*/
/*         <table id="tbl" class="display" cellspacing="0" width="100%">*/
/*             <thead>*/
/*             <tr>*/
/*                 {#<th></th>#}*/
/*                 <th>Id</th>*/
/*                 <th>Project Name</th>*/
/*                 <th>Description</th>*/
/*                 <th>Project Icon</th>*/
/*                 <th width="18%">Created date</th>*/
/*                 <th>Actions</th>*/
/*             </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             {% for project in projects %}*/
/*                 <tr>*/
/*                     {#<td></td>#}*/
/*                     <td>{{ project.id }}</td>*/
/*                     <td>{{ project.name }}</td>*/
/*                     <td>{{ project.short_desctiption }}</td>*/
/*                     <td>{{ project.project_icon }}</td>*/
/*                     <td>{{ project.created_on }}</td>*/
/*                     <td>*/
/*                         <a title="Recycle" data-recycleid="{{ project.id }}" data-recyclename="{{ project.name }}" data-recycleinfo="Project Description: {{ project.desc }}" onclick="recycle(this);" href="#">*/
/*                             <i class="fa fa-trash fa-2x"></i>*/
/*                         </a>*/
/*                         {% set edit_url = ['project','edit',project.id] %}*/
/*                         <a title="Edit Project" href="{{ site_url(edit_url)}}">*/
/*                             <i class="fa fa-pencil-square-o fa-2x"></i>*/
/*                         </a>*/
/*                     </td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*         {{ include('twig/manage/inc/recycle.twig', { 'data': data }) }}*/
/* */
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     {{ include("twig/manage/inc/recycle-js.twig", {'data':data}) }}*/
/* */
/*     <script>*/
/*         $(document).ready(function () {*/
/*             $('#tbl').DataTable();*/
/*         });*/
/*         $('#tbl')*/
/*                 .removeClass( 'display' )*/
/*                 .addClass('table table-striped table-bordered');*/
/* */
/*     </script>*/
/* {% endblock %}*/
/* */
