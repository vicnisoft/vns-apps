<?php

/* twig/francaisfacile/list-video.twig */
class __TwigTemplate_0320612b8268d228472a0085822a74f5d0d4d1cfc4271725d7ba4cc52eb0cf65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/francaisfacile/list-video.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/buttons.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/select.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/editor.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/js/dataTables.editor.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "    <div class=\"col-md-12\">
        </br>
        <div class=\"form-inline\">
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-info\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "create_url", array()), "html", null, true);
        echo "\">Create New</a>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-default\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">Filters</a>
                </div>
            </div>
        </div>
        </br>
    </div>

    <div class=\"col-md-12\">
        <table id=\"tbl\" class=\"display\" cellspacing=\"0\" width=\"100%\">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Content</th>
                <th>Icon</th>
                <th>Youtube ID</th>
                <th>Category</th>
                <th width=\"18%\">Created On</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["videos"]) ? $context["videos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["video"]) {
            // line 47
            echo "                <tr>
                    <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "content", array()), "html", null, true);
            echo "</td>
                    ";
            // line 52
            echo "                    <td>
                        <img src=\" ";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "icon", array()), "html", null, true);
            echo "\" class=\"img-thumbnail\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "name", array()), "html", null, true);
            echo "\" width=\"100\" height=\"auto\">
                    </td>
                    <td>";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "youtubeId", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["video"], "category", array()), "name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "createdOn", array()), "html", null, true);
            echo "</td>
                    <td>
                        <a title=\"Recycle\" data-recycleid=\"";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "id", array()), "html", null, true);
            echo "\" data-recyclename=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "name", array()), "html", null, true);
            echo "\" data-recycleinfo=\"Video Content: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "short_content", array()), "html", null, true);
            echo "\" onclick=\"recycle(this);\" href=\"#\">
                            <i class=\"fa fa-trash fa-2x\"></i>
                        </a>
                        ";
            // line 62
            $context["edit_url"] = array(0 => "video", 1 => "edit", 2 => $this->getAttribute($context["video"], "id", array()));
            // line 63
            echo "                        <a title=\"Edit Video\" href=\"";
            echo twig_escape_filter($this->env, site_url((isset($context["edit_url"]) ? $context["edit_url"] : null)), "html", null, true);
            echo "\">
                            <i class=\"fa fa-pencil-square-o fa-2x\"></i>
                        </a>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['video'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "            </tbody>
        </table>
        ";
        // line 71
        echo twig_include($this->env, $context, "twig/manage/inc/recycle.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    </div>
";
    }

    // line 76
    public function block_javascripts($context, array $blocks = array())
    {
        // line 77
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 78
        echo twig_include($this->env, $context, "twig/manage/inc/recycle-js.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    <script>
        \$(document).ready(function () {
            \$('#tbl').DataTable();
        });
        \$('#tbl')
                .removeClass( 'display' )
                .addClass('table table-striped table-bordered');

    </script>
";
    }

    public function getTemplateName()
    {
        return "twig/francaisfacile/list-video.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 78,  184 => 77,  181 => 76,  173 => 71,  169 => 69,  156 => 63,  154 => 62,  144 => 59,  139 => 57,  135 => 56,  131 => 55,  124 => 53,  121 => 52,  117 => 50,  113 => 49,  109 => 48,  106 => 47,  102 => 46,  72 => 19,  65 => 14,  62 => 13,  56 => 10,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/*     <link href="{{ base_url("public/vendor/datatables/css/jquery.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/buttons.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/select.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/editor.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <script src="{{ base_url("public/vendor/datatables/js/dataTables.editor.min.js") }}"></script>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="col-md-12">*/
/*         </br>*/
/*         <div class="form-inline">*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-info" href="{{ data.create_url }}">Create New</a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-default" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Filters</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         </br>*/
/*     </div>*/
/* */
/*     <div class="col-md-12">*/
/*         <table id="tbl" class="display" cellspacing="0" width="100%">*/
/*             <thead>*/
/*             <tr>*/
/*                 <th>Id</th>*/
/*                 <th>Name</th>*/
/*                 <th>Content</th>*/
/*                 <th>Icon</th>*/
/*                 <th>Youtube ID</th>*/
/*                 <th>Category</th>*/
/*                 <th width="18%">Created On</th>*/
/*                 <th>Actions</th>*/
/*             </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             {% for video in videos %}*/
/*                 <tr>*/
/*                     <td>{{ video.id }}</td>*/
/*                     <td>{{ video.name }}</td>*/
/*                     <td>{{ video.content }}</td>*/
/*                     {#<td>{{ video.icon }}</td>#}*/
/*                     <td>*/
/*                         <img src=" {{ video.icon }}" class="img-thumbnail" alt="{{ video.name }}" width="100" height="auto">*/
/*                     </td>*/
/*                     <td>{{ video.youtubeId }}</td>*/
/*                     <td>{{ video.category.name }}</td>*/
/*                     <td>{{ video.createdOn }}</td>*/
/*                     <td>*/
/*                         <a title="Recycle" data-recycleid="{{ video.id }}" data-recyclename="{{ video.name }}" data-recycleinfo="Video Content: {{ video.short_content }}" onclick="recycle(this);" href="#">*/
/*                             <i class="fa fa-trash fa-2x"></i>*/
/*                         </a>*/
/*                         {% set edit_url = ['video','edit',video.id] %}*/
/*                         <a title="Edit Video" href="{{ site_url(edit_url)}}">*/
/*                             <i class="fa fa-pencil-square-o fa-2x"></i>*/
/*                         </a>*/
/*                     </td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*         {{ include('twig/manage/inc/recycle.twig', { 'data': data }) }}*/
/* */
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     {{ include("twig/manage/inc/recycle-js.twig", {'data':data}) }}*/
/* */
/*     <script>*/
/*         $(document).ready(function () {*/
/*             $('#tbl').DataTable();*/
/*         });*/
/*         $('#tbl')*/
/*                 .removeClass( 'display' )*/
/*                 .addClass('table table-striped table-bordered');*/
/* */
/*     </script>*/
/* {% endblock %}*/
/* */
