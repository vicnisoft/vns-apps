<?php

/* twig/manage/list-user.twig */
class __TwigTemplate_a1de22e686b14670979c85a94965430c304e3638ea5296be5f2df9373709ee52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/manage/list-user.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/buttons.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/select.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/editor.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/js/dataTables.editor.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "    <div class=\"col-md-12\">
        </br>
        <div class=\"form-inline\">
            <div class=\"form-group\">
                <div class=\"form-group\">
                    ";
        // line 20
        echo "                    <a class=\"form-control btn btn-primary\" href=\"";
        echo twig_escape_filter($this->env, site_url("user/add"), "html", null, true);
        echo "\">Create New</a>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-default\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">Filters</a>
                </div>
            </div>
        </div>
        </br>
    </div>

    <div id=\"collapseOne\" class=\"panel-collapse collapse\">
        <div class=\"panel-body\">
            <p>Some text here</p>
            <p>Some text here</p>
            <p>Some text here</p>
            <p>Some text here</p>
        </div>
    </div>

    <div class=\"col-md-12\">
        <table id=\"tblUser\" class=\"display\" cellspacing=\"0\" width=\"100%\">
            <thead>
            <tr>
                <th></th>
                <th>Username</th>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th width=\"18%\">Created date</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 57
            echo "                ";
            // line 58
            echo "                <tr>
                    <td>";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "last_name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "first_name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "phone", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "created_date", array()), "html", null, true);
            echo "</td>
                    <td>
                        <a title=\"Recycle\" data-recycleid=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "\" data-recyclename=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()), "html", null, true);
            echo "\" data-recycleinfo=\"User: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()), "html", null, true);
            echo "\" onclick=\"recycle(this);\" href=\"#\">
                            <i class=\"fa fa-trash fa-2x\"></i>
                        </a>
                        ";
            // line 70
            $context["edit_url"] = array(0 => "user", 1 => "edit", 2 => $this->getAttribute($context["user"], "id", array()));
            // line 71
            echo "                        <a title=\"Edit User\" href=\"";
            echo twig_escape_filter($this->env, site_url((isset($context["edit_url"]) ? $context["edit_url"] : null)), "html", null, true);
            echo "\">
                            <i class=\"fa fa-pencil-square-o fa-2x\"></i>
                        </a>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "            </tbody>
        </table>
        ";
        // line 79
        echo twig_include($this->env, $context, "twig/manage/inc/recycle.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    </div>
";
    }

    // line 84
    public function block_javascripts($context, array $blocks = array())
    {
        // line 85
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 86
        echo twig_include($this->env, $context, "twig/manage/inc/recycle-js.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "
    <script>
        \$(document).ready(function () {
            \$('#tblUser').DataTable();
            ";
        // line 91
        echo "        });
        \$('#tblUser')
                .removeClass( 'display' )
                .addClass('table table-striped table-bordered');

    </script>
";
    }

    public function getTemplateName()
    {
        return "twig/manage/list-user.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 91,  195 => 86,  190 => 85,  187 => 84,  179 => 79,  175 => 77,  162 => 71,  160 => 70,  150 => 67,  145 => 65,  141 => 64,  137 => 63,  133 => 62,  129 => 61,  125 => 60,  121 => 59,  118 => 58,  116 => 57,  112 => 56,  72 => 20,  65 => 14,  62 => 13,  56 => 10,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/*     <link href="{{ base_url("public/vendor/datatables/css/jquery.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/buttons.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/select.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/editor.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <script src="{{ base_url("public/vendor/datatables/js/dataTables.editor.min.js") }}"></script>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="col-md-12">*/
/*         </br>*/
/*         <div class="form-inline">*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     {#<a class="form-control btn btn-primary" data-toggle="collapse" data-target="#accordion" href="#collapseOne">Create New</a>#}*/
/*                     <a class="form-control btn btn-primary" href="{{ site_url('user/add') }}">Create New</a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-default" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Filters</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         </br>*/
/*     </div>*/
/* */
/*     <div id="collapseOne" class="panel-collapse collapse">*/
/*         <div class="panel-body">*/
/*             <p>Some text here</p>*/
/*             <p>Some text here</p>*/
/*             <p>Some text here</p>*/
/*             <p>Some text here</p>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="col-md-12">*/
/*         <table id="tblUser" class="display" cellspacing="0" width="100%">*/
/*             <thead>*/
/*             <tr>*/
/*                 <th></th>*/
/*                 <th>Username</th>*/
/*                 <th>Last Name</th>*/
/*                 <th>First Name</th>*/
/*                 <th>Email</th>*/
/*                 <th>Phone</th>*/
/*                 <th width="18%">Created date</th>*/
/*                 <th>Actions</th>*/
/*             </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             {% for user in users %}*/
/*                 {#<li>{{ user.username|e }}</li>#}*/
/*                 <tr>*/
/*                     <td>{{ user.id }}</td>*/
/*                     <td>{{ user.username }}</td>*/
/*                     <td>{{ user.last_name }}</td>*/
/*                     <td>{{ user.first_name }}</td>*/
/*                     <td>{{ user.email }}</td>*/
/*                     <td>{{ user.phone }}</td>*/
/*                     <td>{{ user.created_date }}</td>*/
/*                     <td>*/
/*                         <a title="Recycle" data-recycleid="{{ user.id }}" data-recyclename="{{ user.username }}" data-recycleinfo="User: {{ user.username }}" onclick="recycle(this);" href="#">*/
/*                             <i class="fa fa-trash fa-2x"></i>*/
/*                         </a>*/
/*                         {% set edit_url = ['user','edit',user.id] %}*/
/*                         <a title="Edit User" href="{{ site_url(edit_url)}}">*/
/*                             <i class="fa fa-pencil-square-o fa-2x"></i>*/
/*                         </a>*/
/*                     </td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*         {{ include('twig/manage/inc/recycle.twig', { 'data': data }) }}*/
/* */
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     {{ include("twig/manage/inc/recycle-js.twig", {'data':data}) }}*/
/*     <script>*/
/*         $(document).ready(function () {*/
/*             $('#tblUser').DataTable();*/
/*             {#showLastLogin({{ user.last_login }})#}*/
/*         });*/
/*         $('#tblUser')*/
/*                 .removeClass( 'display' )*/
/*                 .addClass('table table-striped table-bordered');*/
/* */
/*     </script>*/
/* {% endblock %}*/
/* */
