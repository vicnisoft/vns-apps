<?php

/* twig/layout/footer.twig */
class __TwigTemplate_e56dc1355b180bc47c7633ed889640ba0f9ce0986808d636e66020a9421e5e0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"footer-sec\">
    ";
        // line 2
        if ((isset($context["project_name"]) ? $context["project_name"] : null)) {
            echo twig_escape_filter($this->env, (isset($context["project_name"]) ? $context["project_name"] : null), "html", null, true);
            echo " |";
        } else {
            echo "Vicnisoft |";
        }
        // line 3
        echo "    ";
        if ((isset($context["time"]) ? $context["time"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["time"]) ? $context["time"] : null), "html", null, true);
            echo " ";
        }
        // line 4
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "twig/layout/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 4,  29 => 3,  22 => 2,  19 => 1,);
    }
}
/* <div id="footer-sec">*/
/*     {% if project_name %}{{ project_name }} |{% else %}Vicnisoft |{% endif %}*/
/*     {% if time %} {{ time }} {% endif %}*/
/* </div>*/
