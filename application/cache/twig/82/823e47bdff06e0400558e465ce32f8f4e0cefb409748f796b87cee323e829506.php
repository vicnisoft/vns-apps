<?php

/* twig/manage/inc/recycle-js.twig */
class __TwigTemplate_495e03ffa3c8c0c6f340f0b4e0c5c035bfea8f84aae061d2958c1f78efae34f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\">
    function recycle(element) {
        \$(\"#recyclename\").text(\$(element).data(\"recyclename\"));
        \$(\"#recycleinfo\").text(\$(element).data(\"recycleinfo\"));
        \$(\"#recycleid\").val(\$(element).data(\"recycleid\"));
        \$(\"#recycle-warning\").html(\$(element).data(\"recycle-warning\"));
        \$(\"#recycleModal\").find(\"#save-recycle\").data('type', \"single\");

        if (\$(element).data(\"recycle-warning\") != \"\") {
            \$(\"#recycle-warning\").show();
        } else {
            \$(\"#recycle-warning\").hide();
        }
        \$(\"#recycleModal\").modal('show');
    }
    function massRecycle(element) {
        \$(\"#recyclename\").text(\"Multiple selection\")
        \$(\"#recycleModal\").find(\"#save-recycle\").data('type', \"mass\").data(\"formid\", \$(element).data(\"formid\"));
        // Clear data that might have been set by single recycle type
        \$(\"#recycle-warning\").html(\$(element).data(\"recycle-warning\"));
        \$(\"#recycleinfo\").text('');
        \$(\"#recycleid\").val('');
        if (\$(element).data(\"recycle-warning\") != \"\") {
            \$(\"#recycle-warning\").show();
        } else {
            \$(\"#recycle-warning\").hide();
        }
        \$(\"#recycleModal\").modal('show');
    }
    function sendRecycle(element){
        if(\$(element).data(\"type\") === \"single\"){
            \$(\"#recycleModal\").find(\"#recycleForm\").submit();
        } else if(\$(element).data(\"type\") === \"mass\"){
            \$(\"#\"+\$(element).data(\"formid\")).submit();
        }
    }
</script>
";
    }

    public function getTemplateName()
    {
        return "twig/manage/inc/recycle-js.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <script type="text/javascript">*/
/*     function recycle(element) {*/
/*         $("#recyclename").text($(element).data("recyclename"));*/
/*         $("#recycleinfo").text($(element).data("recycleinfo"));*/
/*         $("#recycleid").val($(element).data("recycleid"));*/
/*         $("#recycle-warning").html($(element).data("recycle-warning"));*/
/*         $("#recycleModal").find("#save-recycle").data('type', "single");*/
/* */
/*         if ($(element).data("recycle-warning") != "") {*/
/*             $("#recycle-warning").show();*/
/*         } else {*/
/*             $("#recycle-warning").hide();*/
/*         }*/
/*         $("#recycleModal").modal('show');*/
/*     }*/
/*     function massRecycle(element) {*/
/*         $("#recyclename").text("Multiple selection")*/
/*         $("#recycleModal").find("#save-recycle").data('type', "mass").data("formid", $(element).data("formid"));*/
/*         // Clear data that might have been set by single recycle type*/
/*         $("#recycle-warning").html($(element).data("recycle-warning"));*/
/*         $("#recycleinfo").text('');*/
/*         $("#recycleid").val('');*/
/*         if ($(element).data("recycle-warning") != "") {*/
/*             $("#recycle-warning").show();*/
/*         } else {*/
/*             $("#recycle-warning").hide();*/
/*         }*/
/*         $("#recycleModal").modal('show');*/
/*     }*/
/*     function sendRecycle(element){*/
/*         if($(element).data("type") === "single"){*/
/*             $("#recycleModal").find("#recycleForm").submit();*/
/*         } else if($(element).data("type") === "mass"){*/
/*             $("#"+$(element).data("formid")).submit();*/
/*         }*/
/*     }*/
/* </script>*/
/* */
