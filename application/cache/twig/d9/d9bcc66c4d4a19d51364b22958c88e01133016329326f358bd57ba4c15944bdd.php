<?php

/* twig/pages/tools.twig */
class __TwigTemplate_18c5fe7d7f9eaa9866e5f032c4ed01b1afbaf7d25b0bdeb17ece12a2abd1c038 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/pages/tools.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "
    <div>
        <a class=\"form-control btn btn-primary\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("tools/generate_classes"), "html", null, true);
        echo "\">Generate Classes Entities</a>

        <h3>Extract data from url</h3>
        <form name=\"tools\" method=\"post\" action=\"";
        // line 15
        echo twig_escape_filter($this->env, site_url("tools"), "html", null, true);
        echo "\" class=\"form-horizontal\" novalidate=\"novalidate\">
            <div class=\"hide alert alert-danger\" id=\"edit-sku-errors\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Error!</strong>
\t\t\t<span class=\"message\">
\t\t\t</span>
            </div>
            <div class=\"hide alert alert-success\" id=\"edit-sku-success\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Success!</strong>
                <span class=\"message\"></span>
            </div>
            <fieldset>
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <div class=\"form-group\">
                            <label for=\"tool_url\" class=\"control-label col-md-2 required\">
                                URL
                            </label>
                            <div class=\"col-md-10\">
                                <input id=\"tool_url\" name=\"tool_url\" required=\"required\" class=\"form-control\" type=\"text\">
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <hr>
            <div class=\"form_buttons pull-right\">
                <button type=\"submit\" id=\"tool_submit\" name=\"tool_submit\" class=\"btn btn-primary\">
                    Save </button>
                <button type=\"submit\" id=\"tool_cancel\" name=\"tool_cancel\" formnovalidate=\"formnovalidate\" class=\"btn btn-default\">
                    Cancel </button>
            </div>
    </div>

";
    }

    // line 52
    public function block_javascripts($context, array $blocks = array())
    {
        // line 53
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

";
    }

    public function getTemplateName()
    {
        return "twig/pages/tools.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 53,  96 => 52,  56 => 15,  50 => 12,  46 => 10,  43 => 9,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     <div>*/
/*         <a class="form-control btn btn-primary" href="{{ site_url('tools/generate_classes') }}">Generate Classes Entities</a>*/
/* */
/*         <h3>Extract data from url</h3>*/
/*         <form name="tools" method="post" action="{{ site_url('tools') }}" class="form-horizontal" novalidate="novalidate">*/
/*             <div class="hide alert alert-danger" id="edit-sku-errors">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Error!</strong>*/
/* 			<span class="message">*/
/* 			</span>*/
/*             </div>*/
/*             <div class="hide alert alert-success" id="edit-sku-success">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Success!</strong>*/
/*                 <span class="message"></span>*/
/*             </div>*/
/*             <fieldset>*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <div class="form-group">*/
/*                             <label for="tool_url" class="control-label col-md-2 required">*/
/*                                 URL*/
/*                             </label>*/
/*                             <div class="col-md-10">*/
/*                                 <input id="tool_url" name="tool_url" required="required" class="form-control" type="text">*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </fieldset>*/
/*             <hr>*/
/*             <div class="form_buttons pull-right">*/
/*                 <button type="submit" id="tool_submit" name="tool_submit" class="btn btn-primary">*/
/*                     Save </button>*/
/*                 <button type="submit" id="tool_cancel" name="tool_cancel" formnovalidate="formnovalidate" class="btn btn-default">*/
/*                     Cancel </button>*/
/*             </div>*/
/*     </div>*/
/* */
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* */
