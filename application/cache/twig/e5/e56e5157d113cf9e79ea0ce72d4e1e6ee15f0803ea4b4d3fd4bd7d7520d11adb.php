<?php

/* twig/francaisfacile/edit-dialogue.twig */
class __TwigTemplate_fa57bf60a640013ece94cd33302df6595d22b815bf1587e08ba0f267caaea609 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/francaisfacile/edit-dialogue.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div>
        <form name=\"dialogue\" method=\"post\" action=\"\"  novalidate=\"novalidate\">
            <div class=\"";
        // line 12
        if ( !array_key_exists("error_message", $context)) {
            echo "hide";
        }
        echo " alert alert-danger\" id=\"edit-project-errors\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Error!</strong>
                <span class=\"message\">";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null), "html", null, true);
        echo "</span>
            </div>
            <div class=\"";
        // line 17
        if ( !array_key_exists("success_masage", $context)) {
            echo "hide";
        }
        echo " alert alert-success\" id=\"edit-project-errors\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Success!</strong>
                <span class=\"message\">";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["success_masage"]) ? $context["success_masage"] : null), "html", null, true);
        echo "</span>
            </div>
            <fieldset>
                <div class=\"row\">
                    ";
        // line 24
        if ( !array_key_exists("dialogue", $context)) {
            // line 25
            echo "                        <div class=\"col-md-12 \">
                            <div class=\"panel panel-primary\">
                                <div class=\"panel-heading\">
                                    AAA
                                </div>
                                <div class=\"panel-body\">
                                    <div class=\"col-md-6\">
                                        <div class=\"form-group\">
                                            <label for=\"dialogue_name\" class=\"control-label  required\">
                                                Name *
                                            </label>
                                            <input id=\"dialogue_name\" name=\"dialogue[name]\" required=\"required\" class=\"form-control\" type=\"text\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"dialogue_audio\" class=\"control-label  required\">
                                                Audio *
                                            </label>
                                            <input id=\"dialogue_audio\" name=\"dialogue[audio_url]\" required=\"required\" class=\"form-control\" type=\"url\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"dialogue_icon\" class=\"control-label  required\">
                                                Icon *
                                            </label>
                                            <input id=\"dialogue_icon\" name=\"dialogue[icon]\" required=\"required\" class=\"form-control\" type=\"url\">
                                        </div>
                                    </div>
                                    <div class=\"col-md-6\">
                                        <div class=\"form-group\">
                                            <label for=\"dialogue_category\" class=\"control-label  required\">
                                                Category *
                                            </label>
                                            <select id=\"dialogue_category\" name=\"dialogue[category_id]\" class=\"form-control\">
                                                ";
            // line 57
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 58
                echo "                                                    <option  selected=\"false\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "value", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "key", array()), "html", null, true);
                echo "</option>
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 60
            echo "                                            </select>
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"dialogue_content\" class=\"control-label  required\">
                                                Content *
                                            </label>
                                            <textarea  id=\"dialogue_content\" name=\"dialogue[content]\" required=\"required\" class=\" form-control\"></textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    ";
        } else {
            // line 74
            echo "                        <div class=\"col-md-12\">

                            <div class=\"panel panel-primary\">
                                <div class=\"panel-heading\">
                                    Create a dialogue
                                </div>
                                <div class=\"panel-body\">
                                    <div class=\"col-md-6\">
                                        <div class=\"form-group\">
                                                <input id=\"dialogue_id\" name=\"dialogue[dialogue_id]\" required=\"required\" class=\"form-control\" type=\"hidden\" value=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dialogue"]) ? $context["dialogue"] : null), "id", array()), "html", null, true);
            echo "\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"dialogue_name\" class=\"control-label  required\">
                                                Name *
                                            </label>
                                            <input id=\"dialogue_name\" name=\"dialogue[name]\" required=\"required\" class=\"form-control\" type=\"text\" value=\"";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dialogue"]) ? $context["dialogue"] : null), "name", array()), "html", null, true);
            echo "\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"dialogue_audio\" class=\"control-label  required\">
                                                Audio *
                                            </label>
                                            <input id=\"dialogue_audio\" name=\"dialogue[audio_url]\" required=\"required\" class=\"form-control\" type=\"url\" value=\"";
            // line 95
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dialogue"]) ? $context["dialogue"] : null), "audio_url", array()), "html", null, true);
            echo "\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"dialogue_icon\" class=\"control-label  required\">
                                                Icon *
                                            </label>
                                            <input id=\"dialogue_icon\" name=\"dialogue[icon]\" required=\"required\" class=\"form-control\" type=\"url\" value=\"";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dialogue"]) ? $context["dialogue"] : null), "icon", array()), "html", null, true);
            echo "\">
                                        </div>
                                    </div>
                                    <div class=\"col-md-6\">
                                        <div class=\"form-group\">
                                            <label for=\"dialogue_category\" class=\"control-label  required\">
                                                Category *
                                            </label>
                                            <select class=\"form-control\" id=\"dialogue_category\" name=\"dialogue[category_id]\">
                                                ";
            // line 110
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 111
                echo "                                                    <option  selected=\"false\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "value", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "key", array()), "html", null, true);
                echo "</option>
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 113
            echo "                                            </select>
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"dialogue_content\" class=\"control-label  required\">
                                                Content *
                                            </label>
                                            <textarea  id=\"dialogue_content\" name=\"dialogue[content]\" required=\"required\" class=\" form-control\">
                                                ";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dialogue"]) ? $context["dialogue"] : null), "content", array()), "html", null, true);
            echo "
                                            </textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    ";
        }
        // line 129
        echo "            </fieldset>
            <hr>
            <div class=\"form_buttons pull-right\">
                <button type=\"submit\"  class=\"btn btn-primary\">
                    Save
                </button>
                <a  href=\"";
        // line 135
        echo twig_escape_filter($this->env, site_url("dialogue/index"), "html", null, true);
        echo "\" class=\"btn btn-default\">
                    Cancel
                </a>
            </div>
            <div class=\"clear\"></div>
            <input id=\"user__token\" name=\"user[_token]\" class=\"form-control\" value=\"v6JhOQJ_IZ8RBG1SClYzx8AdxWCdqQ4WjKWylUalX4g\" type=\"hidden\"></form>
        <div class=\"row\">
            <div class=\"col-md-6\"></div>
        </div>
    </div>
";
    }

    // line 147
    public function block_javascripts($context, array $blocks = array())
    {
        // line 148
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "twig/francaisfacile/edit-dialogue.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 148,  254 => 147,  239 => 135,  231 => 129,  219 => 120,  210 => 113,  199 => 111,  195 => 110,  183 => 101,  174 => 95,  165 => 89,  156 => 83,  145 => 74,  129 => 60,  118 => 58,  114 => 57,  80 => 25,  78 => 24,  71 => 20,  63 => 17,  58 => 15,  50 => 12,  46 => 10,  43 => 9,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div>*/
/*         <form name="dialogue" method="post" action=""  novalidate="novalidate">*/
/*             <div class="{% if (error_message is not defined ) %}hide{% endif %} alert alert-danger" id="edit-project-errors">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Error!</strong>*/
/*                 <span class="message">{{ error_message }}</span>*/
/*             </div>*/
/*             <div class="{% if (success_masage is not defined ) %}hide{% endif %} alert alert-success" id="edit-project-errors">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Success!</strong>*/
/*                 <span class="message">{{ success_masage }}</span>*/
/*             </div>*/
/*             <fieldset>*/
/*                 <div class="row">*/
/*                     {% if dialogue is not defined %}*/
/*                         <div class="col-md-12 ">*/
/*                             <div class="panel panel-primary">*/
/*                                 <div class="panel-heading">*/
/*                                     AAA*/
/*                                 </div>*/
/*                                 <div class="panel-body">*/
/*                                     <div class="col-md-6">*/
/*                                         <div class="form-group">*/
/*                                             <label for="dialogue_name" class="control-label  required">*/
/*                                                 Name **/
/*                                             </label>*/
/*                                             <input id="dialogue_name" name="dialogue[name]" required="required" class="form-control" type="text">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="dialogue_audio" class="control-label  required">*/
/*                                                 Audio **/
/*                                             </label>*/
/*                                             <input id="dialogue_audio" name="dialogue[audio_url]" required="required" class="form-control" type="url">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="dialogue_icon" class="control-label  required">*/
/*                                                 Icon **/
/*                                             </label>*/
/*                                             <input id="dialogue_icon" name="dialogue[icon]" required="required" class="form-control" type="url">*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     <div class="col-md-6">*/
/*                                         <div class="form-group">*/
/*                                             <label for="dialogue_category" class="control-label  required">*/
/*                                                 Category **/
/*                                             </label>*/
/*                                             <select id="dialogue_category" name="dialogue[category_id]" class="form-control">*/
/*                                                 {% for category in categories %}*/
/*                                                     <option  selected="false" value="{{ category.value }}">{{ category.key }}</option>*/
/*                                                 {% endfor %}*/
/*                                             </select>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="dialogue_content" class="control-label  required">*/
/*                                                 Content **/
/*                                             </label>*/
/*                                             <textarea  id="dialogue_content" name="dialogue[content]" required="required" class=" form-control"></textarea>*/
/*                                         </div>*/
/* */
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% else %}*/
/*                         <div class="col-md-12">*/
/* */
/*                             <div class="panel panel-primary">*/
/*                                 <div class="panel-heading">*/
/*                                     Create a dialogue*/
/*                                 </div>*/
/*                                 <div class="panel-body">*/
/*                                     <div class="col-md-6">*/
/*                                         <div class="form-group">*/
/*                                                 <input id="dialogue_id" name="dialogue[dialogue_id]" required="required" class="form-control" type="hidden" value="{{ dialogue.id }}">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="dialogue_name" class="control-label  required">*/
/*                                                 Name **/
/*                                             </label>*/
/*                                             <input id="dialogue_name" name="dialogue[name]" required="required" class="form-control" type="text" value="{{ dialogue.name }}">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="dialogue_audio" class="control-label  required">*/
/*                                                 Audio **/
/*                                             </label>*/
/*                                             <input id="dialogue_audio" name="dialogue[audio_url]" required="required" class="form-control" type="url" value="{{ dialogue.audio_url }}">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="dialogue_icon" class="control-label  required">*/
/*                                                 Icon **/
/*                                             </label>*/
/*                                             <input id="dialogue_icon" name="dialogue[icon]" required="required" class="form-control" type="url" value="{{ dialogue.icon }}">*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     <div class="col-md-6">*/
/*                                         <div class="form-group">*/
/*                                             <label for="dialogue_category" class="control-label  required">*/
/*                                                 Category **/
/*                                             </label>*/
/*                                             <select class="form-control" id="dialogue_category" name="dialogue[category_id]">*/
/*                                                 {% for category in categories %}*/
/*                                                     <option  selected="false" value="{{ category.value }}">{{ category.key }}</option>*/
/*                                                 {% endfor %}*/
/*                                             </select>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="dialogue_content" class="control-label  required">*/
/*                                                 Content **/
/*                                             </label>*/
/*                                             <textarea  id="dialogue_content" name="dialogue[content]" required="required" class=" form-control">*/
/*                                                 {{ dialogue.content }}*/
/*                                             </textarea>*/
/*                                         </div>*/
/* */
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% endif %}*/
/*             </fieldset>*/
/*             <hr>*/
/*             <div class="form_buttons pull-right">*/
/*                 <button type="submit"  class="btn btn-primary">*/
/*                     Save*/
/*                 </button>*/
/*                 <a  href="{{ site_url('dialogue/index') }}" class="btn btn-default">*/
/*                     Cancel*/
/*                 </a>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/*             <input id="user__token" name="user[_token]" class="form-control" value="v6JhOQJ_IZ8RBG1SClYzx8AdxWCdqQ4WjKWylUalX4g" type="hidden"></form>*/
/*         <div class="row">*/
/*             <div class="col-md-6"></div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/* {% endblock %}*/
/* */
