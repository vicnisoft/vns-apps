<?php

/* twig/manage/inc/recycle.twig */
class __TwigTemplate_ff6c7fd5797694c65248efb5ec929259d648ac1032922006b17eca4df73d612e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"recycleModal\" class=\"modal fade\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                <p class=\"bg-danger\">ATTENTION!</p>
                ";
        // line 7
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "active", array())) {
            // line 8
            echo "                    <p class=\"bg-primary\">
                        CONFIRM DELETE
                    </p>
                ";
        } else {
            // line 12
            echo "                    <p class=\"bg-primary\">
                        CONFIRM RESTORE
                    </p>
                ";
        }
        // line 16
        echo "            </div>
            <div class=\"modal-body\">
                <div class=\"alert alert-warning\" role=\"alert\" id=\"recycle-warning\"></div>
                ";
        // line 19
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "active", array())) {
            // line 20
            echo "                    <p>You are about to DELETE ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "recycletype", array()), "html", null, true);
            echo "</p>
                ";
        } else {
            // line 22
            echo "                    <p>You are about to RESTORE ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "recycletype", array()), "html", null, true);
            echo "</p>
                ";
        }
        // line 24
        echo "                <p id=\"recyclename\"></p>
                <p id=\"recycleinfo\"></p>
            </div>
            <div class=\"modal-footer\">
                <form action=\"\" method=\"post\" id=\"recycleForm\">
                    <input type=\"hidden\" name=\"recycleid\" id=\"recycleid\" value=\"\" />
                    <input type=\"hidden\" name=\"active\" id=\"active\" value=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "active", array()), "html", null, true);
        echo "\" />
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                    <button type=\"button\" data-type=\"\" onClick=\"javascript:sendRecycle(this);\" id=\"save-recycle\" class=\"btn btn-primary\">";
        // line 32
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "active", array())) {
            echo "Disable";
        } else {
            echo "Enable";
        }
        echo "</button>
                </form>
            </div>
        </div>
    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "twig/manage/inc/recycle.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 32,  68 => 30,  60 => 24,  54 => 22,  48 => 20,  46 => 19,  41 => 16,  35 => 12,  29 => 8,  27 => 7,  19 => 1,);
    }
}
/* <div id="recycleModal" class="modal fade">*/
/*     <div class="modal-dialog">*/
/*         <div class="modal-content">*/
/*             <div class="modal-header">*/
/*                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                 <p class="bg-danger">ATTENTION!</p>*/
/*                 {% if data.active %}*/
/*                     <p class="bg-primary">*/
/*                         CONFIRM DELETE*/
/*                     </p>*/
/*                 {% else %}*/
/*                     <p class="bg-primary">*/
/*                         CONFIRM RESTORE*/
/*                     </p>*/
/*                 {% endif %}*/
/*             </div>*/
/*             <div class="modal-body">*/
/*                 <div class="alert alert-warning" role="alert" id="recycle-warning"></div>*/
/*                 {% if data.active %}*/
/*                     <p>You are about to DELETE {{ data.recycletype }}</p>*/
/*                 {% else %}*/
/*                     <p>You are about to RESTORE {{ data.recycletype }}</p>*/
/*                 {% endif %}*/
/*                 <p id="recyclename"></p>*/
/*                 <p id="recycleinfo"></p>*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <form action="" method="post" id="recycleForm">*/
/*                     <input type="hidden" name="recycleid" id="recycleid" value="" />*/
/*                     <input type="hidden" name="active" id="active" value="{{ data.active }}" />*/
/*                     <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>*/
/*                     <button type="button" data-type="" onClick="javascript:sendRecycle(this);" id="save-recycle" class="btn btn-primary">{% if data.active %}Disable{% else %}Enable{% endif %}</button>*/
/*                 </form>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* */
