<?php

/* twig/pages/tools.twig */
class __TwigTemplate_d78398581f6176c9944ccfa8324f04518381c2a685858583a45f326a5b3917c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/pages/tools.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "
    <div>
        <a class=\"form-control btn btn-primary\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("tools/generate_classes"), "html", null, true);
        echo "\">Generate Classes Entities</a>

        <h3>Extract data from url</h3>
        <form name=\"tools\" method=\"post\" action=\"";
        // line 15
        echo twig_escape_filter($this->env, site_url("tools"), "html", null, true);
        echo "\" class=\"form-horizontal\" novalidate=\"novalidate\">
            <div class=\"";
        // line 16
        if ( !array_key_exists("error_message", $context)) {
            echo "hide";
        }
        echo " alert alert-danger\" id=\"edit-project-errors\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Error!</strong>
                <span class=\"message\">";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null), "html", null, true);
        echo "</span>
            </div>
            <div class=\"";
        // line 21
        if ( !array_key_exists("success_masage", $context)) {
            echo "hide";
        }
        echo " alert alert-success\" id=\"edit-project-errors\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Success!</strong>
                <span class=\"message\">";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["success_masage"]) ? $context["success_masage"] : null), "html", null, true);
        echo "</span>
            </div>
            <fieldset>
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <div class=\"form-group\">
                            <label for=\"tool_url\" class=\"control-label col-md-2 required\">
                                URL
                            </label>
                            <div class=\"col-md-10\">
                                <input id=\"tool_url\" name=\"tool_url\" required=\"required\" class=\"form-control\" type=\"text\">
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <hr>
            <div class=\"form_buttons pull-right\">
                <button type=\"submit\" id=\"tool_submit\" name=\"tool_submit\" class=\"btn btn-primary\">
                    Save </button>
                <button type=\"submit\" id=\"tool_cancel\" name=\"tool_cancel\" formnovalidate=\"formnovalidate\" class=\"btn btn-default\">
                    Cancel </button>
            </div>
    </div>

";
    }

    // line 51
    public function block_javascripts($context, array $blocks = array())
    {
        // line 52
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

";
    }

    public function getTemplateName()
    {
        return "twig/pages/tools.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 52,  111 => 51,  81 => 24,  73 => 21,  68 => 19,  60 => 16,  56 => 15,  50 => 12,  46 => 10,  43 => 9,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     <div>*/
/*         <a class="form-control btn btn-primary" href="{{ site_url('tools/generate_classes') }}">Generate Classes Entities</a>*/
/* */
/*         <h3>Extract data from url</h3>*/
/*         <form name="tools" method="post" action="{{ site_url('tools') }}" class="form-horizontal" novalidate="novalidate">*/
/*             <div class="{% if (error_message is not defined ) %}hide{% endif %} alert alert-danger" id="edit-project-errors">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Error!</strong>*/
/*                 <span class="message">{{ error_message }}</span>*/
/*             </div>*/
/*             <div class="{% if (success_masage is not defined ) %}hide{% endif %} alert alert-success" id="edit-project-errors">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Success!</strong>*/
/*                 <span class="message">{{ success_masage }}</span>*/
/*             </div>*/
/*             <fieldset>*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <div class="form-group">*/
/*                             <label for="tool_url" class="control-label col-md-2 required">*/
/*                                 URL*/
/*                             </label>*/
/*                             <div class="col-md-10">*/
/*                                 <input id="tool_url" name="tool_url" required="required" class="form-control" type="text">*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </fieldset>*/
/*             <hr>*/
/*             <div class="form_buttons pull-right">*/
/*                 <button type="submit" id="tool_submit" name="tool_submit" class="btn btn-primary">*/
/*                     Save </button>*/
/*                 <button type="submit" id="tool_cancel" name="tool_cancel" formnovalidate="formnovalidate" class="btn btn-default">*/
/*                     Cancel </button>*/
/*             </div>*/
/*     </div>*/
/* */
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* */
