<?php

/* twig/manage/project/list_project.twig */
class __TwigTemplate_82b71ebd5514190e5f8e9658519c59fce71fbf14941ee7fef8612a474687095e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/manage/project/list_project.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/buttons.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/select.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/editor.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/js/dataTables.editor.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "    <div class=\"col-md-12\">
        </br>
        <div class=\"form-inline\">
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-primary\" href=\"/createUser\">Create New</a>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-default\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">Filters</a>
                </div>
            </div>
        </div>
        </br>
    </div>

    <div class=\"col-md-12\">
        <table id=\"tbl\" class=\"display\" cellspacing=\"0\" width=\"100%\">
            <thead>
            <tr>
                <th></th>
                <th>Username</th>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th width=\"18%\">Created date</th>
            </tr>
            </thead>
            <tbody>
            ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 46
            echo "                <li>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()));
            echo "</li>
                <tr>
                    <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "last_name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "first_name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "phone", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "created_date", array()), "html", null, true);
            echo "</td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "            </tbody>
        </table>
    </div>
";
    }

    // line 62
    public function block_javascripts($context, array $blocks = array())
    {
        // line 63
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(document).ready(function () {
            \$('#tbl').DataTable();
        });
        \$('#tbl')
                .removeClass( 'display' )
                .addClass('table table-striped table-bordered');

    </script>
";
    }

    public function getTemplateName()
    {
        return "twig/manage/project/list_project.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 63,  148 => 62,  141 => 57,  132 => 54,  128 => 53,  124 => 52,  120 => 51,  116 => 50,  112 => 49,  108 => 48,  102 => 46,  98 => 45,  65 => 14,  62 => 13,  56 => 10,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/*     <link href="{{ base_url("public/vendor/datatables/css/jquery.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/buttons.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/select.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/editor.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <script src="{{ base_url("public/vendor/datatables/js/dataTables.editor.min.js") }}"></script>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="col-md-12">*/
/*         </br>*/
/*         <div class="form-inline">*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-primary" href="/createUser">Create New</a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-default" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Filters</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         </br>*/
/*     </div>*/
/* */
/*     <div class="col-md-12">*/
/*         <table id="tbl" class="display" cellspacing="0" width="100%">*/
/*             <thead>*/
/*             <tr>*/
/*                 <th></th>*/
/*                 <th>Username</th>*/
/*                 <th>Last Name</th>*/
/*                 <th>First Name</th>*/
/*                 <th>Email</th>*/
/*                 <th>Phone</th>*/
/*                 <th width="18%">Created date</th>*/
/*             </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             {% for user in users %}*/
/*                 <li>{{ user.username|e }}</li>*/
/*                 <tr>*/
/*                     <td>{{ user.id }}</td>*/
/*                     <td>{{ user.username }}</td>*/
/*                     <td>{{ user.last_name }}</td>*/
/*                     <td>{{ user.first_name }}</td>*/
/*                     <td>{{ user.email }}</td>*/
/*                     <td>{{ user.phone }}</td>*/
/*                     <td>{{ user.created_date }}</td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script>*/
/*         $(document).ready(function () {*/
/*             $('#tbl').DataTable();*/
/*         });*/
/*         $('#tbl')*/
/*                 .removeClass( 'display' )*/
/*                 .addClass('table table-striped table-bordered');*/
/* */
/*     </script>*/
/* {% endblock %}*/
/* */
