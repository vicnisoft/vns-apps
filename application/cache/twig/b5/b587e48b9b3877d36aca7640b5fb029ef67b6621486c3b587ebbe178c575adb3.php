<?php

/* twig/francaisfacile/list-category.twig */
class __TwigTemplate_3cdea7bd74f23813076cd96669eae5a1f577a1c80c2bb84595da21b111ae1ff1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/francaisfacile/list-category.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/buttons.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/select.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/editor.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/js/dataTables.editor.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "    <div class=\"col-md-12\">
        </br>
        <div class=\"form-inline\">
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-info\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, site_url("category/add"), "html", null, true);
        echo "\">Create New</a>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-default\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">Filters</a>
                </div>
            </div>
        </div>
        </br>
    </div>

    <div class=\"col-md-12\">
        <table id=\"tbl\" class=\"display\" cellspacing=\"0\" width=\"100%\">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Category Name</th>
                    <th>Description</th>
                    <th>Category Icon</th>
                    <th width=\"18%\">Created On</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 45
            echo "                    <tr>
                        <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "description", array()), "html", null, true);
            echo "</td>
                        ";
            // line 50
            echo "                        <td>
                            <img src=\" ";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "iconUrl", array()), "html", null, true);
            echo "\" class=\"img-thumbnail\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "\" width=\"100\" height=\"auto\">
                        </td>
                        <td>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "createdOn", array()), "html", null, true);
            echo "</td>
                        <td>
                            <a title=\"Recycle\" data-recycleid=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "\" data-recyclename=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "\" data-recycleinfo=\"Project Description: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "description", array()), "html", null, true);
            echo "\" onclick=\"recycle(this);\" href=\"#\">
                                <i class=\"fa fa-trash fa-2x\"></i>
                            </a>
                            ";
            // line 58
            $context["edit_url"] = array(0 => "category", 1 => "edit", 2 => $this->getAttribute($context["category"], "id", array()));
            // line 59
            echo "                            <a title=\"Edit Category\" href=\"";
            echo twig_escape_filter($this->env, site_url((isset($context["edit_url"]) ? $context["edit_url"] : null)), "html", null, true);
            echo "\">
                                <i class=\"fa fa-pencil-square-o fa-2x\"></i>
                            </a>
                        </td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "                </tbody>
        </table>
        ";
        // line 67
        echo twig_include($this->env, $context, "twig/manage/inc/recycle.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    </div>
";
    }

    // line 72
    public function block_javascripts($context, array $blocks = array())
    {
        // line 73
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 74
        echo twig_include($this->env, $context, "twig/manage/inc/recycle-js.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    <script>
        \$(document).ready(function () {
            \$('#tbl').DataTable();
        });
        \$('#tbl')
                .removeClass( 'display' )
                .addClass('table table-striped table-bordered');

    </script>
";
    }

    public function getTemplateName()
    {
        return "twig/francaisfacile/list-category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 74,  174 => 73,  171 => 72,  163 => 67,  159 => 65,  146 => 59,  144 => 58,  134 => 55,  129 => 53,  122 => 51,  119 => 50,  115 => 48,  111 => 47,  107 => 46,  104 => 45,  100 => 44,  72 => 19,  65 => 14,  62 => 13,  56 => 10,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/*     <link href="{{ base_url("public/vendor/datatables/css/jquery.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/buttons.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/select.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/editor.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <script src="{{ base_url("public/vendor/datatables/js/dataTables.editor.min.js") }}"></script>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="col-md-12">*/
/*         </br>*/
/*         <div class="form-inline">*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-info" href="{{ site_url('category/add') }}">Create New</a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-default" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Filters</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         </br>*/
/*     </div>*/
/* */
/*     <div class="col-md-12">*/
/*         <table id="tbl" class="display" cellspacing="0" width="100%">*/
/*                 <thead>*/
/*                 <tr>*/
/*                     <th>Id</th>*/
/*                     <th>Category Name</th>*/
/*                     <th>Description</th>*/
/*                     <th>Category Icon</th>*/
/*                     <th width="18%">Created On</th>*/
/*                     <th>Actions</th>*/
/*                 </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                 {% for category in categories %}*/
/*                     <tr>*/
/*                         <td>{{ category.id }}</td>*/
/*                         <td>{{ category.name }}</td>*/
/*                         <td>{{ category.description }}</td>*/
/*                         {#<td>{{ category.icon }}</td>#}*/
/*                         <td>*/
/*                             <img src=" {{ category.iconUrl }}" class="img-thumbnail" alt="{{ category.name }}" width="100" height="auto">*/
/*                         </td>*/
/*                         <td>{{ category.createdOn }}</td>*/
/*                         <td>*/
/*                             <a title="Recycle" data-recycleid="{{ category.id }}" data-recyclename="{{ category.name }}" data-recycleinfo="Project Description: {{ category.description }}" onclick="recycle(this);" href="#">*/
/*                                 <i class="fa fa-trash fa-2x"></i>*/
/*                             </a>*/
/*                             {% set edit_url = ['category','edit',category.id] %}*/
/*                             <a title="Edit Category" href="{{ site_url(edit_url)}}">*/
/*                                 <i class="fa fa-pencil-square-o fa-2x"></i>*/
/*                             </a>*/
/*                         </td>*/
/*                     </tr>*/
/*                 {% endfor %}*/
/*                 </tbody>*/
/*         </table>*/
/*         {{ include('twig/manage/inc/recycle.twig', { 'data': data }) }}*/
/* */
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     {{ include("twig/manage/inc/recycle-js.twig", {'data':data}) }}*/
/* */
/*     <script>*/
/*         $(document).ready(function () {*/
/*             $('#tbl').DataTable();*/
/*         });*/
/*         $('#tbl')*/
/*                 .removeClass( 'display' )*/
/*                 .addClass('table table-striped table-bordered');*/
/* */
/*     </script>*/
/* {% endblock %}*/
/* */
