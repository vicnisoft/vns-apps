<?php

/* twig/manage/user/edit-user.twig */
class __TwigTemplate_a96cb133599a078d1c5f3768157a832265287e4f8d4bae3e0142602a919b5f5c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/manage/user/edit-user.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div>
        <form name=\"user\" method=\"post\" action=\"\" class=\"form-horizontal\" novalidate=\"novalidate\">
            <div class=\"hide alert alert-danger\" id=\"edit-sku-errors\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Error!</strong>
\t\t\t<span class=\"message\">


\t\t\t</span>
            </div>
            <div class=\"hide alert alert-success\" id=\"edit-sku-success\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Success!</strong>
                <span class=\"message\"></span>
            </div>

            <fieldset>
                <div class=\"row\">
                    <div class=\"col-md-6\">
                        <h3>User Info</h3>
                        <div class=\"form-group\">
                            <label for=\"user_username\" class=\"control-label col-sm-3 required\">
                                Username *
                                <span class=\"help-block\">
                                    <a title=\"\" data-original-title=\"\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" data-title=\"Usernames may only contain lower case letters, numbers, &quot;-&quot; and &quot;_&quot;\">
                                        <span class=\"glyphicon glyphicon-info-sign\"></span>
                                    </a>
                                </span>
                            </label>
                            <div class=\"col-sm-9\">
                                <input id=\"user_username\" name=\"user[username]\" required=\"required\" class=\"form-control\" type=\"text\">
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"user_plainpassword_first\" class=\"control-label col-sm-3 required\">
                                Password *
                                <span class=\"help-block\">
                                    <a title=\"\" data-original-title=\"\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" data-title=\"Password must contain a minimum of 8 characters and at least one number\">
                                        <span class=\"glyphicon glyphicon-info-sign\"></span>
                                    </a>
                                </span>
                            </label>
                            <div class=\"col-sm-9\">
                                <input id=\"user_plainpassword_first\" name=\"user[plainpassword][first]\" required=\"required\" class=\"form-control\" type=\"password\">
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"user_plainpassword_second\" class=\"control-label col-sm-3 required\">
                                Confirm Password *
                            </label>
                            <div class=\"col-sm-9\">
                                <input id=\"user_plainpassword_second\" name=\"user[plainpassword][second]\" required=\"required\" class=\"form-control\" type=\"password\">
                            </div>
                        </div>
                       <div class=\"form-group\">
                           <label class=\"radio_btn control-label col-sm-3 required\">
                                Gender
                            </label>
                           <div class=\"col-sm-9\">
                               <div class=\"radio\">
                                   <label class=\"radio_btn \">
                                       <input id=\"user_gender_0\" name=\"user[gender]\" required=\"required\" class=\"\" value=\"Male\" type=\"radio\">
                                        Male
                                    </label>
                               </div>
                               <div class=\"radio\">
                                   <label class=\"radio_btn \">
                                       <input id=\"user_gender_1\" name=\"user[gender]\" required=\"required\" class=\"\" value=\"Female\" type=\"radio\">
                                        Female
                                    </label>
                               </div>
                           </div>
                       </div>
                        <div class=\"form-group\">
                            <label for=\"user_language\" class=\"control-label col-sm-3 required\">
                                Language
                            </label>
                            <div class=\"col-sm-9\">
                                <select id=\"user_language\" name=\"user[language]\" required=\"required\" class=\" form-control\">
                                    <option value=\"\" selected=\"selected\"></option>
                                    <option value=\"English\">English</option>
                                    <option value=\"French\">French</option>
                                    <option value=\"Japanese\">Japanese</option>
                                    <option value=\"Korean\">Korean</option>
                                    <option value=\"Chinese\">Chinese</option>
                                    <option value=\"Spanish\">Spanish</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-6\">
                        <h3>Contact Info</h3>
                        <div class=\"form-group\">
                            <label for=\"user_email_first\" class=\"control-label col-sm-3 required\">
                                Email *
                            </label>
                            <div class=\"col-sm-9\">
                                <div class=\"input-group\">
                                    <span class=\"input-group-addon\">@</span>
                                    <input id=\"user_email_first\" name=\"user[email][first]\" required=\"required\" class=\"form-control\" type=\"email\">
                                </div>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"user_email_second\" class=\"control-label col-sm-3 required\">
                                Confirm Email *
                            </label>
                            <div class=\"col-sm-9\">
                                <div class=\"input-group\">
                                    <span class=\"input-group-addon\">@
                                    </span>
                                    <input id=\"user_email_second\" name=\"user[email][second]\" required=\"required\" class=\"form-control\" type=\"email\">
                                </div>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"user_phone\" class=\"control-label col-sm-3 required\">
                                Phone Number
                            </label>
                            <div class=\"col-sm-9\">
                                <input id=\"user_phone\" name=\"user[phone]\" required=\"required\" class=\"form-control\" type=\"text\">
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"user_cellphone\" class=\"control-label col-sm-3 required\">
                                Cell Number
                            </label>
                            <div class=\"col-sm-9\">
                                <input id=\"user_cellphone\" name=\"user[cellphone]\" required=\"required\" class=\"form-control\" type=\"text\">
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"user_skypeid\" class=\"control-label col-sm-3 required\">
                                Skype ID
                            </label>
                            <div class=\"col-sm-9\">
                                <input id=\"user_skypeid\" name=\"user[skypeid]\" required=\"required\" class=\"form-control\" type=\"text\">
                            </div>
                        </div>
                        <h3>Location Info</h3>
                        <div class=\"form-group\">
                            <label for=\"user_country\" class=\"control-label col-sm-3 required\">
                                Country
                            </label>
                            <div class=\"col-sm-9\">
                                <input id=\"user_country\" name=\"user[country]\" required=\"required\" class=\"form-control\" type=\"text\">
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"user_state\" class=\"control-label col-sm-3 required\">
                                State
                            </label>
                            <div class=\"col-sm-9\">
                                <input id=\"user_state\" name=\"user[state]\" required=\"required\" class=\"form-control\" type=\"text\">
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"user_city\" class=\"control-label col-sm-3 required\">
                                City
                            </label>
                            <div class=\"col-sm-9\">
                                <input id=\"user_city\" name=\"user[city]\" required=\"required\" class=\"form-control\" type=\"text\">
                            </div>
                        </div>
                    </div>
                </div>

            </fieldset>

            <hr>
            <div class=\"form_buttons pull-right\">
                <button type=\"submit\" id=\"user_submit\" name=\"user[submit]\" class=\"btn btn-primary\">
                    Save</button> <button type=\"submit\" id=\"user_cancel\" name=\"user[cancel]\" formnovalidate=\"formnovalidate\" class=\"btn btn-default\">
                    Cancel</button>
            </div>
            <div class=\"clear\"></div>


            <input id=\"user__token\" name=\"user[_token]\" class=\"form-control\" value=\"v6JhOQJ_IZ8RBG1SClYzx8AdxWCdqQ4WjKWylUalX4g\" type=\"hidden\"></form>
        <div class=\"row\">
            <div class=\"col-md-6\"></div>
        </div>
    </div>
";
    }

    // line 195
    public function block_javascripts($context, array $blocks = array())
    {
        // line 196
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "twig/manage/user/edit-user.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 196,  233 => 195,  46 => 10,  43 => 9,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div>*/
/*         <form name="user" method="post" action="" class="form-horizontal" novalidate="novalidate">*/
/*             <div class="hide alert alert-danger" id="edit-sku-errors">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Error!</strong>*/
/* 			<span class="message">*/
/* */
/* */
/* 			</span>*/
/*             </div>*/
/*             <div class="hide alert alert-success" id="edit-sku-success">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Success!</strong>*/
/*                 <span class="message"></span>*/
/*             </div>*/
/* */
/*             <fieldset>*/
/*                 <div class="row">*/
/*                     <div class="col-md-6">*/
/*                         <h3>User Info</h3>*/
/*                         <div class="form-group">*/
/*                             <label for="user_username" class="control-label col-sm-3 required">*/
/*                                 Username **/
/*                                 <span class="help-block">*/
/*                                     <a title="" data-original-title="" href="#" data-toggle="tooltip" data-placement="top" data-title="Usernames may only contain lower case letters, numbers, &quot;-&quot; and &quot;_&quot;">*/
/*                                         <span class="glyphicon glyphicon-info-sign"></span>*/
/*                                     </a>*/
/*                                 </span>*/
/*                             </label>*/
/*                             <div class="col-sm-9">*/
/*                                 <input id="user_username" name="user[username]" required="required" class="form-control" type="text">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="user_plainpassword_first" class="control-label col-sm-3 required">*/
/*                                 Password **/
/*                                 <span class="help-block">*/
/*                                     <a title="" data-original-title="" href="#" data-toggle="tooltip" data-placement="top" data-title="Password must contain a minimum of 8 characters and at least one number">*/
/*                                         <span class="glyphicon glyphicon-info-sign"></span>*/
/*                                     </a>*/
/*                                 </span>*/
/*                             </label>*/
/*                             <div class="col-sm-9">*/
/*                                 <input id="user_plainpassword_first" name="user[plainpassword][first]" required="required" class="form-control" type="password">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="user_plainpassword_second" class="control-label col-sm-3 required">*/
/*                                 Confirm Password **/
/*                             </label>*/
/*                             <div class="col-sm-9">*/
/*                                 <input id="user_plainpassword_second" name="user[plainpassword][second]" required="required" class="form-control" type="password">*/
/*                             </div>*/
/*                         </div>*/
/*                        <div class="form-group">*/
/*                            <label class="radio_btn control-label col-sm-3 required">*/
/*                                 Gender*/
/*                             </label>*/
/*                            <div class="col-sm-9">*/
/*                                <div class="radio">*/
/*                                    <label class="radio_btn ">*/
/*                                        <input id="user_gender_0" name="user[gender]" required="required" class="" value="Male" type="radio">*/
/*                                         Male*/
/*                                     </label>*/
/*                                </div>*/
/*                                <div class="radio">*/
/*                                    <label class="radio_btn ">*/
/*                                        <input id="user_gender_1" name="user[gender]" required="required" class="" value="Female" type="radio">*/
/*                                         Female*/
/*                                     </label>*/
/*                                </div>*/
/*                            </div>*/
/*                        </div>*/
/*                         <div class="form-group">*/
/*                             <label for="user_language" class="control-label col-sm-3 required">*/
/*                                 Language*/
/*                             </label>*/
/*                             <div class="col-sm-9">*/
/*                                 <select id="user_language" name="user[language]" required="required" class=" form-control">*/
/*                                     <option value="" selected="selected"></option>*/
/*                                     <option value="English">English</option>*/
/*                                     <option value="French">French</option>*/
/*                                     <option value="Japanese">Japanese</option>*/
/*                                     <option value="Korean">Korean</option>*/
/*                                     <option value="Chinese">Chinese</option>*/
/*                                     <option value="Spanish">Spanish</option>*/
/*                                 </select>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-6">*/
/*                         <h3>Contact Info</h3>*/
/*                         <div class="form-group">*/
/*                             <label for="user_email_first" class="control-label col-sm-3 required">*/
/*                                 Email **/
/*                             </label>*/
/*                             <div class="col-sm-9">*/
/*                                 <div class="input-group">*/
/*                                     <span class="input-group-addon">@</span>*/
/*                                     <input id="user_email_first" name="user[email][first]" required="required" class="form-control" type="email">*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="user_email_second" class="control-label col-sm-3 required">*/
/*                                 Confirm Email **/
/*                             </label>*/
/*                             <div class="col-sm-9">*/
/*                                 <div class="input-group">*/
/*                                     <span class="input-group-addon">@*/
/*                                     </span>*/
/*                                     <input id="user_email_second" name="user[email][second]" required="required" class="form-control" type="email">*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="user_phone" class="control-label col-sm-3 required">*/
/*                                 Phone Number*/
/*                             </label>*/
/*                             <div class="col-sm-9">*/
/*                                 <input id="user_phone" name="user[phone]" required="required" class="form-control" type="text">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="user_cellphone" class="control-label col-sm-3 required">*/
/*                                 Cell Number*/
/*                             </label>*/
/*                             <div class="col-sm-9">*/
/*                                 <input id="user_cellphone" name="user[cellphone]" required="required" class="form-control" type="text">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="user_skypeid" class="control-label col-sm-3 required">*/
/*                                 Skype ID*/
/*                             </label>*/
/*                             <div class="col-sm-9">*/
/*                                 <input id="user_skypeid" name="user[skypeid]" required="required" class="form-control" type="text">*/
/*                             </div>*/
/*                         </div>*/
/*                         <h3>Location Info</h3>*/
/*                         <div class="form-group">*/
/*                             <label for="user_country" class="control-label col-sm-3 required">*/
/*                                 Country*/
/*                             </label>*/
/*                             <div class="col-sm-9">*/
/*                                 <input id="user_country" name="user[country]" required="required" class="form-control" type="text">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="user_state" class="control-label col-sm-3 required">*/
/*                                 State*/
/*                             </label>*/
/*                             <div class="col-sm-9">*/
/*                                 <input id="user_state" name="user[state]" required="required" class="form-control" type="text">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="user_city" class="control-label col-sm-3 required">*/
/*                                 City*/
/*                             </label>*/
/*                             <div class="col-sm-9">*/
/*                                 <input id="user_city" name="user[city]" required="required" class="form-control" type="text">*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/* */
/*             </fieldset>*/
/* */
/*             <hr>*/
/*             <div class="form_buttons pull-right">*/
/*                 <button type="submit" id="user_submit" name="user[submit]" class="btn btn-primary">*/
/*                     Save</button> <button type="submit" id="user_cancel" name="user[cancel]" formnovalidate="formnovalidate" class="btn btn-default">*/
/*                     Cancel</button>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/* */
/* */
/*             <input id="user__token" name="user[_token]" class="form-control" value="v6JhOQJ_IZ8RBG1SClYzx8AdxWCdqQ4WjKWylUalX4g" type="hidden"></form>*/
/*         <div class="row">*/
/*             <div class="col-md-6"></div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/* {% endblock %}*/
/* */
