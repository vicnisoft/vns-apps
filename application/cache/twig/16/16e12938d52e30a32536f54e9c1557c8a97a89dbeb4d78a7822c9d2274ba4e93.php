<?php

/* twig/layout/navigation.twig */
class __TwigTemplate_4124249d905e95cf578a7914cd01b518ae7c92bb7b2c187d326f211021612bb1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav class=\"navbar navbar-default navbar-cls-top \" role=\"navigation\" style=\"margin-bottom: 0\">
    <div class=\"navbar-header\">
        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".sidebar-collapse\">
            <span class=\"sr-only\">Toggle navigation</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
        </button>
        <a class=\"navbar-brand\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">";
        if ((isset($context["project_name"]) ? $context["project_name"] : null)) {
            echo twig_escape_filter($this->env, (isset($context["project_name"]) ? $context["project_name"] : null), "html", null, true);
            echo "  ";
        } else {
            echo " Vicnisoft ";
        }
        echo "</a>
    </div>

    <div class=\"header-right\">

        <a href=\"message-task.html\" class=\"btn btn-info\" title=\"New Message\"><b>30 </b><i class=\"fa fa-envelope-o fa-2x\"></i></a>
        <a href=\"message-task.html\" class=\"btn btn-primary\" title=\"New Task\"><b>40 </b><i class=\"fa fa-bars fa-2x\"></i></a>
        <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, site_url("auth/logout"), "html", null, true);
        echo "\" class=\"btn btn-danger\" title=\"Logout\"><i class=\"fa fa-exclamation-circle fa-2x\"></i></a>

    </div>
</nav>";
    }

    public function getTemplateName()
    {
        return "twig/layout/navigation.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 16,  29 => 9,  19 => 1,);
    }
}
/* <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">*/
/*     <div class="navbar-header">*/
/*         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">*/
/*             <span class="sr-only">Toggle navigation</span>*/
/*             <span class="icon-bar"></span>*/
/*             <span class="icon-bar"></span>*/
/*             <span class="icon-bar"></span>*/
/*         </button>*/
/*         <a class="navbar-brand" href="{{ site_url() }}">{% if project_name %}{{ project_name }}  {% else %} Vicnisoft {% endif %}</a>*/
/*     </div>*/
/* */
/*     <div class="header-right">*/
/* */
/*         <a href="message-task.html" class="btn btn-info" title="New Message"><b>30 </b><i class="fa fa-envelope-o fa-2x"></i></a>*/
/*         <a href="message-task.html" class="btn btn-primary" title="New Task"><b>40 </b><i class="fa fa-bars fa-2x"></i></a>*/
/*         <a href="{{ site_url("auth/logout") }}" class="btn btn-danger" title="Logout"><i class="fa fa-exclamation-circle fa-2x"></i></a>*/
/* */
/*     </div>*/
/* </nav>*/
