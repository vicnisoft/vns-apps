<?php

/* twig/layout/navigation_side.twig */
class __TwigTemplate_62a047a205d71b18b2bdbc09618ddc30ca23044ddf68a3756c3a418d028caadb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'user_info' => array($this, 'block_user_info'),
            'Menu' => array($this, 'block_Menu'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav class=\"navbar-default navbar-side\" role=\"navigation\">
    <div class=\"sidebar-collapse\">
        <ul class=\"nav\" id=\"main-menu\">
            ";
        // line 4
        $this->displayBlock('user_info', $context, $blocks);
        // line 17
        echo "
        ";
        // line 18
        $this->displayBlock('Menu', $context, $blocks);
        // line 111
        echo "
        </ul>
    </div>
</nav>";
    }

    // line 4
    public function block_user_info($context, array $blocks = array())
    {
        // line 5
        echo "            <li>
                <div class=\"user-img-div\">
                    <img src=\"public/vendor/bs-advance-admin/img/user.png\" class=\"img-thumbnail\" />

                    <div class=\"inner-text\">
                        ";
        // line 10
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "html", null, true);
        } elseif ((isset($context["project_name"]) ? $context["project_name"] : null)) {
            echo twig_escape_filter($this->env, (isset($context["project_name"]) ? $context["project_name"] : null), "html", null, true);
        } else {
            echo "Vicnisoft";
        }
        // line 11
        echo "                        <br />
                        <small id=\"last-time-login\">Last Login : 2 Weeks Ago </small>
                    </div>
                </div>
            </li>
            ";
    }

    // line 18
    public function block_Menu($context, array $blocks = array())
    {
        // line 19
        echo "
            <li>
                <a class=\"active-menu\" href=\"#\"><i class=\"fa fa-dashboard \"></i>Dashboard</a>
            </li>
            <li>
                <a href=\"#\"><i class=\"fa fa-desktop \"></i>Manage <span class=\"fa arrow\"></span></a>
                <ul class=\"nav nav-second-level\">
                    <li>
                        <a href=\"";
        // line 27
        echo twig_escape_filter($this->env, site_url("user/index"), "html", null, true);
        echo "\"><i class=\"fa fa-toggle-on\"></i>Users</a>
                    </li>
                    <li>
                        <a href=\"";
        // line 30
        echo twig_escape_filter($this->env, site_url("project/index"), "html", null, true);
        echo "\"><i class=\"fa fa-bell \"></i>Projects</a>
                    </li>
                    <li>
                        <a href=\"";
        // line 33
        echo twig_escape_filter($this->env, site_url("tools/index"), "html", null, true);
        echo "\"><i class=\"fa fa-circle-o \"></i>Tools</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href=\"#\"><i class=\"fa fa-yelp \"></i>Francais facile <span class=\"fa arrow\"></span></a>
                <ul class=\"nav nav-second-level\">
                    <li>
                        <a href=\"";
        // line 41
        echo twig_escape_filter($this->env, site_url("category/index"), "html", null, true);
        echo "\"><i class=\"fa fa-coffee\"></i>Categories</a>
                    </li>
                    <li>
                        <a href=\"";
        // line 44
        echo twig_escape_filter($this->env, site_url("dialogue/index"), "html", null, true);
        echo "\"><i class=\"fa fa-flash \"></i>Dialogues</a>
                    </li>
                    <li>
                        <a href=\"";
        // line 47
        echo twig_escape_filter($this->env, site_url("text/index"), "html", null, true);
        echo "\"><i class=\"fa fa-key \"></i>Texts</a>
                    </li>
                    <li>
                        <a href=\"";
        // line 50
        echo twig_escape_filter($this->env, site_url("video/index"), "html", null, true);
        echo "\"><i class=\"fa fa-send \"></i>Videos</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href=\"table.html\"><i class=\"fa fa-flash \"></i>Data Tables </a>

            </li>
            <li>
                <a href=\"#\"><i class=\"fa fa-bicycle \"></i>Forms <span class=\"fa arrow\"></span></a>
                <ul class=\"nav nav-second-level\">

                    <li>
                        <a href=\"form.html\"><i class=\"fa fa-desktop \"></i>Basic </a>
                    </li>
                    <li>
                        <a href=\"form-advance.html\"><i class=\"fa fa-code \"></i>Advance</a>
                    </li>


                </ul>
            </li>
            <li>
                <a href=\"gallery.html\"><i class=\"fa fa-anchor \"></i>Gallery</a>
            </li>
            <li>
                <a href=\"error.html\"><i class=\"fa fa-bug \"></i>Error Page</a>
            </li>
            <li>
                <a href=\"login.html\"><i class=\"fa fa-sign-in \"></i>Login Page</a>
            </li>
            <li>
                <a href=\"#\"><i class=\"fa fa-sitemap \"></i>Multilevel Link <span class=\"fa arrow\"></span></a>
                <ul class=\"nav nav-second-level\">
                    <li>
                        <a href=\"#\"><i class=\"fa fa-bicycle \"></i>Second Level Link</a>
                    </li>
                    <li>
                        <a href=\"#\"><i class=\"fa fa-flask \"></i>Second Level Link</a>
                    </li>
                    <li>
                        <a href=\"#\">Second Level Link<span class=\"fa arrow\"></span></a>
                        <ul class=\"nav nav-third-level\">
                            <li>
                                <a href=\"#\"><i class=\"fa fa-plus \"></i>Third Level Link</a>
                            </li>
                            <li>
                                <a href=\"#\"><i class=\"fa fa-comments-o \"></i>Third Level Link</a>
                            </li>

                        </ul>

                    </li>
                </ul>
            </li>

            <li>
                <a href=\"blank.html\"><i class=\"fa fa-square-o \"></i>Blank Page</a>
            </li>

            ";
    }

    public function getTemplateName()
    {
        return "twig/layout/navigation_side.twig";
    }

    public function getDebugInfo()
    {
        return array (  121 => 50,  115 => 47,  109 => 44,  103 => 41,  92 => 33,  86 => 30,  80 => 27,  70 => 19,  67 => 18,  58 => 11,  50 => 10,  43 => 5,  40 => 4,  33 => 111,  31 => 18,  28 => 17,  26 => 4,  21 => 1,);
    }
}
/* <nav class="navbar-default navbar-side" role="navigation">*/
/*     <div class="sidebar-collapse">*/
/*         <ul class="nav" id="main-menu">*/
/*             {% block user_info %}*/
/*             <li>*/
/*                 <div class="user-img-div">*/
/*                     <img src="public/vendor/bs-advance-admin/img/user.png" class="img-thumbnail" />*/
/* */
/*                     <div class="inner-text">*/
/*                         {% if user.username %}{{ user.username }}{% elseif project_name %}{{ project_name }}{% else %}Vicnisoft{% endif %}*/
/*                         <br />*/
/*                         <small id="last-time-login">Last Login : 2 Weeks Ago </small>*/
/*                     </div>*/
/*                 </div>*/
/*             </li>*/
/*             {% endblock %}*/
/* */
/*         {% block Menu %}*/
/* */
/*             <li>*/
/*                 <a class="active-menu" href="#"><i class="fa fa-dashboard "></i>Dashboard</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="#"><i class="fa fa-desktop "></i>Manage <span class="fa arrow"></span></a>*/
/*                 <ul class="nav nav-second-level">*/
/*                     <li>*/
/*                         <a href="{{ site_url('user/index') }}"><i class="fa fa-toggle-on"></i>Users</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="{{ site_url('project/index') }}"><i class="fa fa-bell "></i>Projects</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="{{ site_url('tools/index') }}"><i class="fa fa-circle-o "></i>Tools</a>*/
/*                     </li>*/
/*                 </ul>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="#"><i class="fa fa-yelp "></i>Francais facile <span class="fa arrow"></span></a>*/
/*                 <ul class="nav nav-second-level">*/
/*                     <li>*/
/*                         <a href="{{ site_url('category/index') }}"><i class="fa fa-coffee"></i>Categories</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="{{ site_url('dialogue/index') }}"><i class="fa fa-flash "></i>Dialogues</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="{{ site_url('text/index') }}"><i class="fa fa-key "></i>Texts</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="{{ site_url('video/index') }}"><i class="fa fa-send "></i>Videos</a>*/
/*                     </li>*/
/*                 </ul>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="table.html"><i class="fa fa-flash "></i>Data Tables </a>*/
/* */
/*             </li>*/
/*             <li>*/
/*                 <a href="#"><i class="fa fa-bicycle "></i>Forms <span class="fa arrow"></span></a>*/
/*                 <ul class="nav nav-second-level">*/
/* */
/*                     <li>*/
/*                         <a href="form.html"><i class="fa fa-desktop "></i>Basic </a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="form-advance.html"><i class="fa fa-code "></i>Advance</a>*/
/*                     </li>*/
/* */
/* */
/*                 </ul>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="gallery.html"><i class="fa fa-anchor "></i>Gallery</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="error.html"><i class="fa fa-bug "></i>Error Page</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="login.html"><i class="fa fa-sign-in "></i>Login Page</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="#"><i class="fa fa-sitemap "></i>Multilevel Link <span class="fa arrow"></span></a>*/
/*                 <ul class="nav nav-second-level">*/
/*                     <li>*/
/*                         <a href="#"><i class="fa fa-bicycle "></i>Second Level Link</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="#"><i class="fa fa-flask "></i>Second Level Link</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="#">Second Level Link<span class="fa arrow"></span></a>*/
/*                         <ul class="nav nav-third-level">*/
/*                             <li>*/
/*                                 <a href="#"><i class="fa fa-plus "></i>Third Level Link</a>*/
/*                             </li>*/
/*                             <li>*/
/*                                 <a href="#"><i class="fa fa-comments-o "></i>Third Level Link</a>*/
/*                             </li>*/
/* */
/*                         </ul>*/
/* */
/*                     </li>*/
/*                 </ul>*/
/*             </li>*/
/* */
/*             <li>*/
/*                 <a href="blank.html"><i class="fa fa-square-o "></i>Blank Page</a>*/
/*             </li>*/
/* */
/*             {% endblock %}*/
/* */
/*         </ul>*/
/*     </div>*/
/* </nav>*/
