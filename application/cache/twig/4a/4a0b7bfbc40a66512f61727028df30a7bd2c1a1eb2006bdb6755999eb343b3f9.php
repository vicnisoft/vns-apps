<?php

/* twig/pages/login.twig */
class __TwigTemplate_1f54a7a8a933b1488d82c889ce7789d29a434f91a4a48a66fbe53f7b55a5701d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/pages/login.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div id=\"login-page\">
        <div class=\"row text-center \" style=\"padding-top:100px;\">
            <div class=\"col-md-12\">
                <img src=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url("public/vendor/bs-advance-admin/img/logo-invoice.png"), "html", null, true);
        echo "\" />
            </div>
        </div>
        <div class=\"row \">
            <div class=\"col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1\">

                <div class=\"panel-body\">
                    <form role=\"form\" method=\"post\" action=\"";
        // line 14
        echo twig_escape_filter($this->env, site_url("auth/login"), "html", null, true);
        echo "\">
                        <hr />
                        <br />
                        <div class=\"form-group input-group\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-tag\"  ></i></span>
                            <input type=\"text\" class=\"form-control\" name=\"identity\" value=\"\" id=\"identity\" placeholder=\"Your Username \" />
                        </div>
                        <div class=\"form-group input-group\">
                            <span class=\"input-group-addon\"><i class=\"fa fa-lock\"  ></i></span>
                            <input type=\"password\" class=\"form-control\" name=\"password\" value=\"\" id=\"password\" placeholder=\"Your Password\" />
                        </div>
                        <div class=\"form-group\">
                            <label class=\"checkbox-inline\">
                                <input type=\"checkbox\" name=\"remember\" value=\"1\"  id=\"remember\" /> Remember me
                            </label>
                                            <span class=\"pull-right\">
                                                   <a href=\"index.html\" >Forget password ? </a>
                                            </span>
                        </div>

                        <div class=\"form-group input-group\">

                            <button  class=\"btn btn-primary\" type=\"submit\" name=\"submit\">Login Now </button>
                        </div>

                        <hr />
                        Not register ? <a href=\"index.html\" >click here </a> or go to <a href=\"index.html\">Home</a>
                    </form>
                </div>

            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "twig/pages/login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 14,  36 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block content %}*/
/*     <div id="login-page">*/
/*         <div class="row text-center " style="padding-top:100px;">*/
/*             <div class="col-md-12">*/
/*                 <img src="{{ base_url("public/vendor/bs-advance-admin/img/logo-invoice.png") }}" />*/
/*             </div>*/
/*         </div>*/
/*         <div class="row ">*/
/*             <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">*/
/* */
/*                 <div class="panel-body">*/
/*                     <form role="form" method="post" action="{{ site_url("auth/login") }}">*/
/*                         <hr />*/
/*                         <br />*/
/*                         <div class="form-group input-group">*/
/*                             <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>*/
/*                             <input type="text" class="form-control" name="identity" value="" id="identity" placeholder="Your Username " />*/
/*                         </div>*/
/*                         <div class="form-group input-group">*/
/*                             <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>*/
/*                             <input type="password" class="form-control" name="password" value="" id="password" placeholder="Your Password" />*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label class="checkbox-inline">*/
/*                                 <input type="checkbox" name="remember" value="1"  id="remember" /> Remember me*/
/*                             </label>*/
/*                                             <span class="pull-right">*/
/*                                                    <a href="index.html" >Forget password ? </a>*/
/*                                             </span>*/
/*                         </div>*/
/* */
/*                         <div class="form-group input-group">*/
/* */
/*                             <button  class="btn btn-primary" type="submit" name="submit">Login Now </button>*/
/*                         </div>*/
/* */
/*                         <hr />*/
/*                         Not register ? <a href="index.html" >click here </a> or go to <a href="index.html">Home</a>*/
/*                     </form>*/
/*                 </div>*/
/* */
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* {% endblock %}*/
