<?php

/* twig/francaisfacile/edit-category.twig */
class __TwigTemplate_6b8794cad92607d13b9b1950f666fb120be614f75d1b533bf9fffa79f51ca59d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/francaisfacile/edit-category.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div>
        <form name=\"category\" method=\"post\" action=\"\" class=\"form-horizontal\" novalidate=\"novalidate\">
            <div class=\"";
        // line 12
        if ( !array_key_exists("error_message", $context)) {
            echo "hide";
        }
        echo " alert alert-danger\" id=\"edit-project-errors\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Error!</strong>
                <span class=\"message\">";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null), "html", null, true);
        echo "</span>
            </div>
            <div class=\"";
        // line 17
        if ( !array_key_exists("success_masage", $context)) {
            echo "hide";
        }
        echo " alert alert-success\" id=\"edit-project-errors\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Success!</strong>
                <span class=\"message\">";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["success_masage"]) ? $context["success_masage"] : null), "html", null, true);
        echo "</span>
            </div>
            <fieldset>
                <div class=\"row\">
                    ";
        // line 24
        if ( !array_key_exists("category", $context)) {
            // line 25
            echo "                        <div class=\"col-md-12\">
                            <div class=\"form-group\">
                                <label for=\"category_name\" class=\"control-label col-sm-3 required\">
                                    Category Name *
                                </label>
                                <div class=\"col-sm-9\">
                                    <input id=\"category_name\" name=\"category[name]\" required=\"required\" class=\"form-control\" type=\"text\">
                                </div>
                            </div>
                            <div class=\"form-group\">
                                <label for=\"category_icon\" class=\"control-label col-sm-3 required\">
                                    Category Icon *
                                </label>
                                <div class=\"col-sm-9\">
                                    <input id=\"category_icon\" name=\"category[icon]\" required=\"required\" class=\"form-control\" type=\"url\">
                                </div>
                            </div>
                            <div class=\"form-group\">
                                <label for=\"category_description\" class=\"control-label col-sm-3 required\">
                                    Description *
                                </label>
                                <div class=\"col-sm-9\">
                                    <textarea style=\"height: 81px; overflow-y: hidden;\" id=\"category_description\" name=\"category[description]\" required=\"required\" class=\" form-control\"></textarea>
                                </div>
                            </div>
                        </div>
                    ";
        } else {
            // line 52
            echo "                        <div class=\"col-md-12\">
                            <div class=\"form-group\">
                                <div class=\"col-sm-9\">
                                    <input id=\"project_id\" name=\"category[id]\" required=\"required\" class=\"form-control\" type=\"hidden\" value=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id", array()), "html", null, true);
            echo "\">
                                </div>
                            </div>
                            <div class=\"form-group\">
                                <label for=\"project_name\" class=\"control-label col-sm-3 required\">
                                    Catgory Name *
                                </label>
                                <div class=\"col-sm-9\">
                                    <input id=\"project_name\" name=\"category[name]\" required=\"required\" class=\"form-control\" type=\"text\" value=\"";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "name", array()), "html", null, true);
            echo "\">
                                </div>
                            </div>
                            <div class=\"form-group\">
                                <label for=\"category_icon\" class=\"control-label col-sm-3 required\">
                                    Catgory Icon *
                                </label>
                                <div class=\"col-sm-9\">
                                    <input id=\"category_icon\" name=\"category[icon]\" required=\"required\" class=\"form-control\" type=\"url\" value=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "icon", array()), "html", null, true);
            echo "\">
                                </div>
                            </div>
                            <div class=\"form-group\">
                                <label for=\"category_description\" class=\"control-label col-sm-3 required\">
                                    Description *
                                </label>
                                <div class=\"col-sm-9\">
                                    <textarea style=\"height: 108px; overflow-y: hidden;\" rows=\"3\" id=\"category-description\" name=\"category[description]\" class=\"form-control\">";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "description", array()), "html", null, true);
            echo "
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    ";
        }
        // line 85
        echo "            </fieldset>
            <hr>
            <div class=\"form_buttons pull-right\">
                <button type=\"submit\"  class=\"btn btn-primary\">
                    Save
                </button>
                <a  href=\"";
        // line 91
        echo twig_escape_filter($this->env, site_url("category/index"), "html", null, true);
        echo "\" class=\"btn btn-default\">
                        Cancel
                </a>
            </div>
            <div class=\"clear\"></div>
            <input id=\"user__token\" name=\"user[_token]\" class=\"form-control\" value=\"v6JhOQJ_IZ8RBG1SClYzx8AdxWCdqQ4WjKWylUalX4g\" type=\"hidden\"></form>
        <div class=\"row\">
            <div class=\"col-md-6\"></div>
        </div>
    </div>
";
    }

    // line 103
    public function block_javascripts($context, array $blocks = array())
    {
        // line 104
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "twig/francaisfacile/edit-category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 104,  179 => 103,  164 => 91,  156 => 85,  147 => 79,  136 => 71,  125 => 63,  114 => 55,  109 => 52,  80 => 25,  78 => 24,  71 => 20,  63 => 17,  58 => 15,  50 => 12,  46 => 10,  43 => 9,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div>*/
/*         <form name="category" method="post" action="" class="form-horizontal" novalidate="novalidate">*/
/*             <div class="{% if (error_message is not defined ) %}hide{% endif %} alert alert-danger" id="edit-project-errors">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Error!</strong>*/
/*                 <span class="message">{{ error_message }}</span>*/
/*             </div>*/
/*             <div class="{% if (success_masage is not defined ) %}hide{% endif %} alert alert-success" id="edit-project-errors">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Success!</strong>*/
/*                 <span class="message">{{ success_masage }}</span>*/
/*             </div>*/
/*             <fieldset>*/
/*                 <div class="row">*/
/*                     {% if category is not defined %}*/
/*                         <div class="col-md-12">*/
/*                             <div class="form-group">*/
/*                                 <label for="category_name" class="control-label col-sm-3 required">*/
/*                                     Category Name **/
/*                                 </label>*/
/*                                 <div class="col-sm-9">*/
/*                                     <input id="category_name" name="category[name]" required="required" class="form-control" type="text">*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/*                                 <label for="category_icon" class="control-label col-sm-3 required">*/
/*                                     Category Icon **/
/*                                 </label>*/
/*                                 <div class="col-sm-9">*/
/*                                     <input id="category_icon" name="category[icon]" required="required" class="form-control" type="url">*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/*                                 <label for="category_description" class="control-label col-sm-3 required">*/
/*                                     Description **/
/*                                 </label>*/
/*                                 <div class="col-sm-9">*/
/*                                     <textarea style="height: 81px; overflow-y: hidden;" id="category_description" name="category[description]" required="required" class=" form-control"></textarea>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% else %}*/
/*                         <div class="col-md-12">*/
/*                             <div class="form-group">*/
/*                                 <div class="col-sm-9">*/
/*                                     <input id="project_id" name="category[id]" required="required" class="form-control" type="hidden" value="{{ category.id }}">*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/*                                 <label for="project_name" class="control-label col-sm-3 required">*/
/*                                     Catgory Name **/
/*                                 </label>*/
/*                                 <div class="col-sm-9">*/
/*                                     <input id="project_name" name="category[name]" required="required" class="form-control" type="text" value="{{ category.name }}">*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/*                                 <label for="category_icon" class="control-label col-sm-3 required">*/
/*                                     Catgory Icon **/
/*                                 </label>*/
/*                                 <div class="col-sm-9">*/
/*                                     <input id="category_icon" name="category[icon]" required="required" class="form-control" type="url" value="{{ category.icon }}">*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/*                                 <label for="category_description" class="control-label col-sm-3 required">*/
/*                                     Description **/
/*                                 </label>*/
/*                                 <div class="col-sm-9">*/
/*                                     <textarea style="height: 108px; overflow-y: hidden;" rows="3" id="category-description" name="category[description]" class="form-control">{{ category.description }}*/
/*                                     </textarea>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% endif %}*/
/*             </fieldset>*/
/*             <hr>*/
/*             <div class="form_buttons pull-right">*/
/*                 <button type="submit"  class="btn btn-primary">*/
/*                     Save*/
/*                 </button>*/
/*                 <a  href="{{ site_url('category/index') }}" class="btn btn-default">*/
/*                         Cancel*/
/*                 </a>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/*             <input id="user__token" name="user[_token]" class="form-control" value="v6JhOQJ_IZ8RBG1SClYzx8AdxWCdqQ4WjKWylUalX4g" type="hidden"></form>*/
/*         <div class="row">*/
/*             <div class="col-md-6"></div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/* {% endblock %}*/
/* */
