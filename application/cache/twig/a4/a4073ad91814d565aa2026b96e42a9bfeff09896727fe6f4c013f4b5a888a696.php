<?php

/* twig/francaisfacile/list-category.twig */
class __TwigTemplate_5e5bb21172a4124d8b28e2c872cde482dbd8655f2bbfd2931481c124b9f283ef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/francaisfacile/list-category.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/buttons.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/select.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/editor.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/js/dataTables.editor.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "    <div class=\"col-md-12\">
        </br>
        <div class=\"form-inline\">
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-info\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, site_url("category/add"), "html", null, true);
        echo "\">Create New</a>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-default\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">Filters</a>
                </div>
            </div>
        </div>
        </br>
    </div>

    <div class=\"col-md-12\">
        <table id=\"tbl\" class=\"display\" cellspacing=\"0\" width=\"100%\">
            ";
        // line 33
        if ((isset($context["is_temp"]) ? $context["is_temp"] : null)) {
            // line 34
            echo "                <thead>
                <tr>
                    <th>Id</th>
                    <th>Category Name</th>
                    <th>Description</th>
                    <th>Category Icon</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                ";
            // line 44
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 45
                echo "                    <tr>
                        <td>";
                // line 46
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
                echo "</td>
                        <td>";
                // line 47
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
                echo "</td>
                        <td>";
                // line 48
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "short_description", array()), "html", null, true);
                echo "</td>
                        <td>";
                // line 49
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "icon", array()), "html", null, true);
                echo "</td>
                        <td>
                            <a title=\"Recycle\" data-recycleid=\"";
                // line 51
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
                echo "\" data-recyclename=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
                echo "\" data-recycleinfo=\"Project Description: ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "description", array()), "html", null, true);
                echo "\" onclick=\"recycle(this);\" href=\"#\">
                                <i class=\"fa fa-plus fa-2x\"></i>
                            </a>
                        </td>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "                </tbody>

            ";
        } else {
            // line 60
            echo "                <thead>
                <tr>
                    <th>Id</th>
                    <th>Category Name</th>
                    <th>Description</th>
                    <th>Category Icon</th>
                    <th width=\"18%\">Created On</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                ";
            // line 71
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 72
                echo "                    <tr>
                        <td>";
                // line 73
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
                echo "</td>
                        <td>";
                // line 74
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
                echo "</td>
                        <td>";
                // line 75
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "short_description", array()), "html", null, true);
                echo "</td>
                        <td>";
                // line 76
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "icon", array()), "html", null, true);
                echo "</td>
                        <td>";
                // line 77
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "created_on", array()), "html", null, true);
                echo "</td>
                        <td>
                            <a title=\"Recycle\" data-recycleid=\"";
                // line 79
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
                echo "\" data-recyclename=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
                echo "\" data-recycleinfo=\"Project Description: ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "description", array()), "html", null, true);
                echo "\" onclick=\"recycle(this);\" href=\"#\">
                                <i class=\"fa fa-trash fa-2x\"></i>
                            </a>
                            <a title=\"Edit Category\" href=\"";
                // line 82
                echo twig_escape_filter($this->env, site_url($this->getAttribute($context["category"], "edit_url", array())), "html", null, true);
                echo "\">
                                <i class=\"fa fa-pencil-square-o fa-2x\"></i>
                            </a>
                        </td>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 88
            echo "                </tbody>


            ";
        }
        // line 92
        echo "        </table>
        ";
        // line 93
        echo twig_include($this->env, $context, "twig/manage/inc/recycle.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    </div>
";
    }

    // line 98
    public function block_javascripts($context, array $blocks = array())
    {
        // line 99
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 100
        echo twig_include($this->env, $context, "twig/manage/inc/recycle-js.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    <script>
        \$(document).ready(function () {
            \$('#tbl').DataTable();
        });
        \$('#tbl')
                .removeClass( 'display' )
                .addClass('table table-striped table-bordered');

    </script>
";
    }

    public function getTemplateName()
    {
        return "twig/francaisfacile/list-category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 100,  231 => 99,  228 => 98,  220 => 93,  217 => 92,  211 => 88,  199 => 82,  189 => 79,  184 => 77,  180 => 76,  176 => 75,  172 => 74,  168 => 73,  165 => 72,  161 => 71,  148 => 60,  143 => 57,  127 => 51,  122 => 49,  118 => 48,  114 => 47,  110 => 46,  107 => 45,  103 => 44,  91 => 34,  89 => 33,  72 => 19,  65 => 14,  62 => 13,  56 => 10,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/*     <link href="{{ base_url("public/vendor/datatables/css/jquery.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/buttons.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/select.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/editor.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <script src="{{ base_url("public/vendor/datatables/js/dataTables.editor.min.js") }}"></script>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="col-md-12">*/
/*         </br>*/
/*         <div class="form-inline">*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-info" href="{{ site_url('category/add') }}">Create New</a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-default" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Filters</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         </br>*/
/*     </div>*/
/* */
/*     <div class="col-md-12">*/
/*         <table id="tbl" class="display" cellspacing="0" width="100%">*/
/*             {% if is_temp %}*/
/*                 <thead>*/
/*                 <tr>*/
/*                     <th>Id</th>*/
/*                     <th>Category Name</th>*/
/*                     <th>Description</th>*/
/*                     <th>Category Icon</th>*/
/*                     <th>Actions</th>*/
/*                 </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                 {% for category in categories %}*/
/*                     <tr>*/
/*                         <td>{{ category.id }}</td>*/
/*                         <td>{{ category.name }}</td>*/
/*                         <td>{{ category.short_description }}</td>*/
/*                         <td>{{ category.icon }}</td>*/
/*                         <td>*/
/*                             <a title="Recycle" data-recycleid="{{ category.id }}" data-recyclename="{{ category.name }}" data-recycleinfo="Project Description: {{ category.description }}" onclick="recycle(this);" href="#">*/
/*                                 <i class="fa fa-plus fa-2x"></i>*/
/*                             </a>*/
/*                         </td>*/
/*                     </tr>*/
/*                 {% endfor %}*/
/*                 </tbody>*/
/* */
/*             {% else %}*/
/*                 <thead>*/
/*                 <tr>*/
/*                     <th>Id</th>*/
/*                     <th>Category Name</th>*/
/*                     <th>Description</th>*/
/*                     <th>Category Icon</th>*/
/*                     <th width="18%">Created On</th>*/
/*                     <th>Actions</th>*/
/*                 </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                 {% for category in categories %}*/
/*                     <tr>*/
/*                         <td>{{ category.id }}</td>*/
/*                         <td>{{ category.name }}</td>*/
/*                         <td>{{ category.short_description }}</td>*/
/*                         <td>{{ category.icon }}</td>*/
/*                         <td>{{ category.created_on }}</td>*/
/*                         <td>*/
/*                             <a title="Recycle" data-recycleid="{{ category.id }}" data-recyclename="{{ category.name }}" data-recycleinfo="Project Description: {{ category.description }}" onclick="recycle(this);" href="#">*/
/*                                 <i class="fa fa-trash fa-2x"></i>*/
/*                             </a>*/
/*                             <a title="Edit Category" href="{{ site_url(category.edit_url)}}">*/
/*                                 <i class="fa fa-pencil-square-o fa-2x"></i>*/
/*                             </a>*/
/*                         </td>*/
/*                     </tr>*/
/*                 {% endfor %}*/
/*                 </tbody>*/
/* */
/* */
/*             {% endif %}*/
/*         </table>*/
/*         {{ include('twig/manage/inc/recycle.twig', { 'data': data }) }}*/
/* */
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     {{ include("twig/manage/inc/recycle-js.twig", {'data':data}) }}*/
/* */
/*     <script>*/
/*         $(document).ready(function () {*/
/*             $('#tbl').DataTable();*/
/*         });*/
/*         $('#tbl')*/
/*                 .removeClass( 'display' )*/
/*                 .addClass('table table-striped table-bordered');*/
/* */
/*     </script>*/
/* {% endblock %}*/
/* */
