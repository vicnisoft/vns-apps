<?php

/* twig/francaisfacile/edit-video.twig */
class __TwigTemplate_a076bab1257e3e3d21e19402289d5a6726c624bf37db35f42b1f6b4237e21cff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/francaisfacile/edit-video.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div>
        <form name=\"video\" method=\"post\" action=\"\"  novalidate=\"novalidate\">
            <div class=\"";
        // line 12
        if ( !array_key_exists("error_message", $context)) {
            echo "hide";
        }
        echo " alert alert-danger\" id=\"edit-project-errors\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Error!</strong>
                <span class=\"message\">";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null), "html", null, true);
        echo "</span>
            </div>
            <div class=\"";
        // line 17
        if ( !array_key_exists("success_masage", $context)) {
            echo "hide";
        }
        echo " alert alert-success\" id=\"edit-project-errors\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Success!</strong>
                <span class=\"message\">";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["success_masage"]) ? $context["success_masage"] : null), "html", null, true);
        echo "</span>
            </div>
            <fieldset>
                <div class=\"row\">
                    ";
        // line 24
        if ( !array_key_exists("video", $context)) {
            // line 25
            echo "                        <div class=\"col-md-12 \">
                            <div class=\"panel panel-primary\">
                                <div class=\"panel-heading\">
                                    Create new video
                                </div>
                                <div class=\"panel-body\">
                                    <div class=\"col-md-12\">
                                        <div class=\"form-group\">
                                            <label for=\"video_name\" class=\"control-label  required\">
                                                Name *
                                            </label>
                                            <input id=\"video_name\" name=\"video[name]\" required=\"required\" class=\"form-control\" type=\"text\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"video_youtube_id\" class=\"control-label  required\">
                                                Youtube ID *
                                            </label>
                                            <input id=\"video_youtube_id\" name=\"video[youtube_id]\" required=\"required\" class=\"form-control\" type=\"url\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"video_audio\" class=\"control-label  required\">
                                                Audio *
                                            </label>
                                            <input id=\"video_audio\" name=\"video[audio_url]\" required=\"required\" class=\"form-control\" type=\"url\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"video_icon\" class=\"control-label  required\">
                                                Icon *
                                            </label>
                                            <input id=\"video_icon\" name=\"video[icon]\" required=\"required\" class=\"form-control\" type=\"url\">
                                        </div>
                                    </div>
                                    <div class=\"col-md-12\">
                                        <div class=\"form-group\">
                                            <label for=\"video_category\" class=\"control-label  required\">
                                                Category *
                                            </label>
                                            <select id=\"video_category\" name=\"video[category_id]\" class=\"form-control\">
                                                ";
            // line 63
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 64
                echo "                                                    <option  selected=\"false\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "value", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "key", array()), "html", null, true);
                echo "</option>
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 66
            echo "                                            </select>
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"video_content\" class=\"control-label  required\">
                                                Content *
                                            </label>
                                            <textarea  id=\"video_content\" style=\"height: 208px; overflow-y: hidden;\" rows=\"3\" name=\"video[content]\" required=\"required\" class=\" form-control\">
                                            </textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    ";
        } else {
            // line 81
            echo "                        <div class=\"col-md-12\">
                            <div class=\"panel panel-primary\">
                                <div class=\"panel-heading\">
                                    Update video
                                </div>
                                <div class=\"panel-body\">
                                    <div class=\"col-md-12\">
                                        <div class=\"form-group\">
                                            <input id=\"video_id\" name=\"video[video_id]\" required=\"required\" class=\"form-control\" type=\"hidden\" value=\"";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["video"]) ? $context["video"] : null), "id", array()), "html", null, true);
            echo "\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"video_name\" class=\"control-label  required\">
                                                Name *
                                            </label>
                                            <input id=\"video_name\" name=\"video[name]\" required=\"required\" class=\"form-control\" type=\"text\" value=\"";
            // line 95
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["video"]) ? $context["video"] : null), "name", array()), "html", null, true);
            echo "\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"video_youtube_id\" class=\"control-label  required\">
                                                Youtube ID *
                                            </label>
                                            <input id=\"video_youtube_id\" name=\"video[youtube_id]\" required=\"required\" class=\"form-control\" type=\"url\" value=\"";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["video"]) ? $context["video"] : null), "youtubeId", array()), "html", null, true);
            echo "\">
                                        </div>
                                        ";
            // line 104
            echo "                                            ";
            // line 105
            echo "                                                ";
            // line 106
            echo "                                            ";
            // line 107
            echo "                                            ";
            // line 108
            echo "                                        ";
            // line 109
            echo "                                        ";
            // line 110
            echo "                                            ";
            // line 111
            echo "                                                ";
            // line 112
            echo "                                            ";
            // line 113
            echo "                                        ";
            // line 114
            echo "                                    </div>
                                    <div class=\"col-md-12\">
                                        <div class=\"form-group\">
                                            <input name=\"video[old_category]\" class=\"form-control\" type=\"hidden\" value=\"";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["video"]) ? $context["video"] : null), "category", array()), "id", array()), "html", null, true);
            echo "\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"video_icon\" class=\"control-label  required\">
                                                Icon *
                                            </label>
                                            <input id=\"video_icon\" name=\"video[icon]\" required=\"required\" class=\"form-control\" type=\"url\" value=\"";
            // line 123
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["video"]) ? $context["video"] : null), "iconUrl", array()), "html", null, true);
            echo "\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"video_category\" class=\"control-label  required\">
                                                Category *
                                            </label>
                                            <select class=\"form-control\" id=\"video_category\" name=\"video[category_id]\">
                                                ";
            // line 130
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 131
                echo "                                                    ";
                if (($this->getAttribute($this->getAttribute((isset($context["video"]) ? $context["video"] : null), "category", array()), "id", array()) == $this->getAttribute($context["category"], "value", array()))) {
                    // line 132
                    echo "                                                        <option   value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "value", array()), "html", null, true);
                    echo "\" selected>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "key", array()), "html", null, true);
                    echo "</option>
                                                    ";
                } else {
                    // line 134
                    echo "                                                        <option  value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "value", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "key", array()), "html", null, true);
                    echo "</option>
                                                    ";
                }
                // line 136
                echo "                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 137
            echo "                                            </select>
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"video_content\" class=\"control-label  required\">
                                                Content *
                                            </label>
                                            <textarea  id=\"video_content\"  rows=\"10\" name=\"video[content]\" required=\"required\" class=\" form-control\">
                                                ";
            // line 144
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["video"]) ? $context["video"] : null), "content", array()), "html", null, true);
            echo "
                                            </textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    ";
        }
        // line 153
        echo "            </fieldset>
            ";
        // line 155
        echo "            ";
        // line 156
        echo "            ";
        // line 157
        echo "            ";
        // line 158
        echo "            <hr>
            <div class=\"form_buttons pull-right\">
                <button type=\"submit\"  class=\"btn btn-primary\">
                    Save
                </button>
                <a  href=\"";
        // line 163
        echo twig_escape_filter($this->env, site_url("video/index"), "html", null, true);
        echo "\" class=\"btn btn-default\">
                    Cancel
                </a>
            </div>
            <div class=\"clear\"></div>
            <input id=\"user__token\" name=\"user[_token]\" class=\"form-control\" value=\"v6JhOQJ_IZ8RBG1SClYzx8AdxWCdqQ4WjKWylUalX4g\" type=\"hidden\"></form>
        <div class=\"row\">
            <div class=\"col-md-6\"></div>
        </div>
    </div>
";
    }

    // line 175
    public function block_javascripts($context, array $blocks = array())
    {
        // line 176
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, base_url("public/vendor/bootstrap3_player.js"), "html", null, true);
        echo "\"></script>

";
    }

    public function getTemplateName()
    {
        return "twig/francaisfacile/edit-video.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  318 => 177,  313 => 176,  310 => 175,  295 => 163,  288 => 158,  286 => 157,  284 => 156,  282 => 155,  279 => 153,  267 => 144,  258 => 137,  252 => 136,  244 => 134,  236 => 132,  233 => 131,  229 => 130,  219 => 123,  210 => 117,  205 => 114,  203 => 113,  201 => 112,  199 => 111,  197 => 110,  195 => 109,  193 => 108,  191 => 107,  189 => 106,  187 => 105,  185 => 104,  180 => 101,  171 => 95,  162 => 89,  152 => 81,  135 => 66,  124 => 64,  120 => 63,  80 => 25,  78 => 24,  71 => 20,  63 => 17,  58 => 15,  50 => 12,  46 => 10,  43 => 9,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div>*/
/*         <form name="video" method="post" action=""  novalidate="novalidate">*/
/*             <div class="{% if (error_message is not defined ) %}hide{% endif %} alert alert-danger" id="edit-project-errors">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Error!</strong>*/
/*                 <span class="message">{{ error_message }}</span>*/
/*             </div>*/
/*             <div class="{% if (success_masage is not defined ) %}hide{% endif %} alert alert-success" id="edit-project-errors">*/
/*                 <button type="button" class="close" data-dismiss="alert">×</button>*/
/*                 <strong>Success!</strong>*/
/*                 <span class="message">{{ success_masage }}</span>*/
/*             </div>*/
/*             <fieldset>*/
/*                 <div class="row">*/
/*                     {% if video is not defined %}*/
/*                         <div class="col-md-12 ">*/
/*                             <div class="panel panel-primary">*/
/*                                 <div class="panel-heading">*/
/*                                     Create new video*/
/*                                 </div>*/
/*                                 <div class="panel-body">*/
/*                                     <div class="col-md-12">*/
/*                                         <div class="form-group">*/
/*                                             <label for="video_name" class="control-label  required">*/
/*                                                 Name **/
/*                                             </label>*/
/*                                             <input id="video_name" name="video[name]" required="required" class="form-control" type="text">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="video_youtube_id" class="control-label  required">*/
/*                                                 Youtube ID **/
/*                                             </label>*/
/*                                             <input id="video_youtube_id" name="video[youtube_id]" required="required" class="form-control" type="url">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="video_audio" class="control-label  required">*/
/*                                                 Audio **/
/*                                             </label>*/
/*                                             <input id="video_audio" name="video[audio_url]" required="required" class="form-control" type="url">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="video_icon" class="control-label  required">*/
/*                                                 Icon **/
/*                                             </label>*/
/*                                             <input id="video_icon" name="video[icon]" required="required" class="form-control" type="url">*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     <div class="col-md-12">*/
/*                                         <div class="form-group">*/
/*                                             <label for="video_category" class="control-label  required">*/
/*                                                 Category **/
/*                                             </label>*/
/*                                             <select id="video_category" name="video[category_id]" class="form-control">*/
/*                                                 {% for category in categories %}*/
/*                                                     <option  selected="false" value="{{ category.value }}">{{ category.key }}</option>*/
/*                                                 {% endfor %}*/
/*                                             </select>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="video_content" class="control-label  required">*/
/*                                                 Content **/
/*                                             </label>*/
/*                                             <textarea  id="video_content" style="height: 208px; overflow-y: hidden;" rows="3" name="video[content]" required="required" class=" form-control">*/
/*                                             </textarea>*/
/*                                         </div>*/
/* */
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% else %}*/
/*                         <div class="col-md-12">*/
/*                             <div class="panel panel-primary">*/
/*                                 <div class="panel-heading">*/
/*                                     Update video*/
/*                                 </div>*/
/*                                 <div class="panel-body">*/
/*                                     <div class="col-md-12">*/
/*                                         <div class="form-group">*/
/*                                             <input id="video_id" name="video[video_id]" required="required" class="form-control" type="hidden" value="{{ video.id }}">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="video_name" class="control-label  required">*/
/*                                                 Name **/
/*                                             </label>*/
/*                                             <input id="video_name" name="video[name]" required="required" class="form-control" type="text" value="{{ video.name }}">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="video_youtube_id" class="control-label  required">*/
/*                                                 Youtube ID **/
/*                                             </label>*/
/*                                             <input id="video_youtube_id" name="video[youtube_id]" required="required" class="form-control" type="url" value="{{ video.youtubeId }}">*/
/*                                         </div>*/
/*                                         {#<div class="form-group">#}*/
/*                                             {#<label for="video_audio" class="control-label  required">#}*/
/*                                                 {#Audio *#}*/
/*                                             {#</label>#}*/
/*                                             {#<input id="video_audio" name="video[audio_url]" required="required" class="form-control" type="url" value="{{ video.audioUrl }}" onchange="updateAudio()">#}*/
/*                                         {#</div>#}*/
/*                                         {#<div >#}*/
/*                                             {#<audio controls>#}*/
/*                                                 {#<source id="audio_source" src="{{ video.audioUrl }}" type="audio/mpeg" />#}*/
/*                                             {#</audio>#}*/
/*                                         {#</div>#}*/
/*                                     </div>*/
/*                                     <div class="col-md-12">*/
/*                                         <div class="form-group">*/
/*                                             <input name="video[old_category]" class="form-control" type="hidden" value="{{ video.category.id }}">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="video_icon" class="control-label  required">*/
/*                                                 Icon **/
/*                                             </label>*/
/*                                             <input id="video_icon" name="video[icon]" required="required" class="form-control" type="url" value="{{ video.iconUrl }}">*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="video_category" class="control-label  required">*/
/*                                                 Category **/
/*                                             </label>*/
/*                                             <select class="form-control" id="video_category" name="video[category_id]">*/
/*                                                 {% for category in categories %}*/
/*                                                     {% if video.category.id == category.value %}*/
/*                                                         <option   value="{{ category.value }}" selected>{{ category.key }}</option>*/
/*                                                     {% else %}*/
/*                                                         <option  value="{{ category.value }}">{{ category.key }}</option>*/
/*                                                     {% endif %}*/
/*                                                 {% endfor %}*/
/*                                             </select>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label for="video_content" class="control-label  required">*/
/*                                                 Content **/
/*                                             </label>*/
/*                                             <textarea  id="video_content"  rows="10" name="video[content]" required="required" class=" form-control">*/
/*                                                 {{ video.content }}*/
/*                                             </textarea>*/
/*                                         </div>*/
/* */
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% endif %}*/
/*             </fieldset>*/
/*             {#{% for category in categories %}#}*/
/*             {#{{ dump(category.value) }}#}*/
/*             {#{{ dump(video.category_id) }}#}*/
/*             {#{% endfor %}#}*/
/*             <hr>*/
/*             <div class="form_buttons pull-right">*/
/*                 <button type="submit"  class="btn btn-primary">*/
/*                     Save*/
/*                 </button>*/
/*                 <a  href="{{ site_url('video/index') }}" class="btn btn-default">*/
/*                     Cancel*/
/*                 </a>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/*             <input id="user__token" name="user[_token]" class="form-control" value="v6JhOQJ_IZ8RBG1SClYzx8AdxWCdqQ4WjKWylUalX4g" type="hidden"></form>*/
/*         <div class="row">*/
/*             <div class="col-md-6"></div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script src="{{ base_url("public/vendor/bootstrap3_player.js") }}"></script>*/
/* */
/* {% endblock %}*/
/* */
