<?php

/* twig/francaisfacile/list-dialogue.twig */
class __TwigTemplate_76883cb61fffce7a4443ec0401453ece52c6501b7e143c264f505b842e61c147 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("twig/base.twig", "twig/francaisfacile/list-dialogue.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "twig/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/buttons.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/select.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/css/editor.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, base_url("public/vendor/datatables/js/dataTables.editor.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "    <div class=\"col-md-12\">
        </br>
        <div class=\"form-inline\">
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-info\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "create_url", array()), "html", null, true);
        echo "\">Create New</a>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"form-group\">
                    <a class=\"form-control btn btn-default\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">Filters</a>
                </div>
            </div>
        </div>
        </br>
    </div>

    <div class=\"col-md-12\">
        <table id=\"tbl\" class=\"display\" cellspacing=\"0\" width=\"100%\">
            <thead>
            <tr>
                <th>Id</th>
                <th>Dialogue Name</th>
                <th>Content</th>
                <th>Dialogue Icon</th>
                <th>Category</th>
                <th width=\"18%\">Created On</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["dialogues"]) ? $context["dialogues"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["dialogue"]) {
            // line 46
            echo "                <tr>
                    <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["dialogue"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["dialogue"], "name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["dialogue"], "short_content", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["dialogue"], "icon", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["dialogue"], "category_name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["dialogue"], "created_on", array()), "html", null, true);
            echo "</td>
                    <td>
                        <a title=\"Recycle\" data-recycleid=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["dialogue"], "id", array()), "html", null, true);
            echo "\" data-recyclename=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["dialogue"], "name", array()), "html", null, true);
            echo "\" data-recycleinfo=\"Dialogue Content: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["dialogue"], "short_content", array()), "html", null, true);
            echo "\" onclick=\"recycle(this);\" href=\"#\">
                            <i class=\"fa fa-trash fa-2x\"></i>
                        </a>
                        <a title=\"Edit Dialogue\" href=\"";
            // line 57
            echo twig_escape_filter($this->env, site_url($this->getAttribute($context["dialogue"], "edit_url", array())), "html", null, true);
            echo "\">
                            <i class=\"fa fa-pencil-square-o fa-2x\"></i>
                        </a>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dialogue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "            </tbody>
        </table>
        ";
        // line 65
        echo twig_include($this->env, $context, "twig/manage/inc/recycle.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    </div>
";
    }

    // line 70
    public function block_javascripts($context, array $blocks = array())
    {
        // line 71
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 72
        echo twig_include($this->env, $context, "twig/manage/inc/recycle-js.twig", array("data" => (isset($context["data"]) ? $context["data"] : null)));
        echo "

    <script>
        \$(document).ready(function () {
            \$('#tbl').DataTable();
        });
        \$('#tbl')
                .removeClass( 'display' )
                .addClass('table table-striped table-bordered');

    </script>
";
    }

    public function getTemplateName()
    {
        return "twig/francaisfacile/list-dialogue.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 72,  170 => 71,  167 => 70,  159 => 65,  155 => 63,  143 => 57,  133 => 54,  128 => 52,  124 => 51,  120 => 50,  116 => 49,  112 => 48,  108 => 47,  105 => 46,  101 => 45,  72 => 19,  65 => 14,  62 => 13,  56 => 10,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'twig/base.twig' %}*/
/* */
/* {% block stylesheets %}*/
/* */
/*     {{ parent() }}*/
/*     <link href="{{ base_url("public/vendor/datatables/css/jquery.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/buttons.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/select.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <link href="{{ base_url("public/vendor/datatables/css/editor.dataTables.min.css") }}" rel="stylesheet" />*/
/*     <script src="{{ base_url("public/vendor/datatables/js/dataTables.editor.min.js") }}"></script>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="col-md-12">*/
/*         </br>*/
/*         <div class="form-inline">*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-info" href="{{ data.create_url }}">Create New</a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*                 <div class="form-group">*/
/*                     <a class="form-control btn btn-default" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Filters</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         </br>*/
/*     </div>*/
/* */
/*     <div class="col-md-12">*/
/*         <table id="tbl" class="display" cellspacing="0" width="100%">*/
/*             <thead>*/
/*             <tr>*/
/*                 <th>Id</th>*/
/*                 <th>Dialogue Name</th>*/
/*                 <th>Content</th>*/
/*                 <th>Dialogue Icon</th>*/
/*                 <th>Category</th>*/
/*                 <th width="18%">Created On</th>*/
/*                 <th>Actions</th>*/
/*             </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             {% for dialogue in dialogues %}*/
/*                 <tr>*/
/*                     <td>{{ dialogue.id }}</td>*/
/*                     <td>{{ dialogue.name }}</td>*/
/*                     <td>{{ dialogue.short_content }}</td>*/
/*                     <td>{{ dialogue.icon }}</td>*/
/*                     <td>{{ dialogue.category_name }}</td>*/
/*                     <td>{{ dialogue.created_on }}</td>*/
/*                     <td>*/
/*                         <a title="Recycle" data-recycleid="{{ dialogue.id }}" data-recyclename="{{ dialogue.name }}" data-recycleinfo="Dialogue Content: {{ dialogue.short_content }}" onclick="recycle(this);" href="#">*/
/*                             <i class="fa fa-trash fa-2x"></i>*/
/*                         </a>*/
/*                         <a title="Edit Dialogue" href="{{ site_url(dialogue.edit_url)}}">*/
/*                             <i class="fa fa-pencil-square-o fa-2x"></i>*/
/*                         </a>*/
/*                     </td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*         {{ include('twig/manage/inc/recycle.twig', { 'data': data }) }}*/
/* */
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     {{ include("twig/manage/inc/recycle-js.twig", {'data':data}) }}*/
/* */
/*     <script>*/
/*         $(document).ready(function () {*/
/*             $('#tbl').DataTable();*/
/*         });*/
/*         $('#tbl')*/
/*                 .removeClass( 'display' )*/
/*                 .addClass('table table-striped table-bordered');*/
/* */
/*     </script>*/
/* {% endblock %}*/
/* */
