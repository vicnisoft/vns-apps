-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 09, 2016 at 02:59 AM
-- Server version: 5.5.42
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `dev_mysite`
--

-- --------------------------------------------------------

--
-- Table structure for table `ff_texts`
--

CREATE TABLE `ff_videos` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `icon_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `audio_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `inspector_id` int(11) unsigned DEFAULT NULL,
  `recycled` tinyint(1) DEFAULT '0',
  `updated_on` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `approved` tinyint(1) DEFAULT '0',
  `category_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ff_videos`
--

INSERT INTO `ff_videos` (`id`, `name`, `content`, `icon_url`, `audio_url`, `created_on`, `created_by`, `inspector_id`, `recycled`, `updated_on`, `updated_by`, `approved`, `category_id`) VALUES
(1, 'AAABBB', 'casc', 'http://web.vicnisoft.local/index.php/video/add', 'http://web.vicnisoft.local/index.php/video/add', 1456800350, 1, NULL, 1, 1456800815, 1, 0, 8),
(2, 'ACscasa', 'scsac', 'http://web.vicnisoft.local/index.php/video/add', 'http://web.vicnisoft.local/index.php/video/add', 1456800371, 1, NULL, 1, NULL, NULL, 0, 8),
(3, 'knk', 'sacsa', 'http://img.youtube.com/vi/MC3h5JVn9H8/default.jpg', 'http://playerdemo.iainhouston.com/tests/BeBopAliens.mp3', 1456852236, 1, NULL, 0, 1457447153, 1, 0, 25),
(4, 'À la boulangerie (situation 1)', 'Employé : Monsieur bonjour.\r\n\r\nClient : Bonjour, je voudrais trois croissants au beurre, un pain aux raisins et un pain au chocolat, s’il vous plaît.\r\n\r\nEmployé : Oui, ça sera tout ?\r\n\r\nClient : Qu’est-ce que vous avez comme tartes ?\r\n\r\nEmployé : J’ai des tartes aux pommes ou des tartes aux fraises.\r\n\r\nClient : Je vais prendre une tarte aux fraises.\r\n\r\nEmployé : Oui c’est pour combien de personnes ?\r\n\r\nClient : Pour six personnes.\r\n\r\nEmployé : Voilà monsieur. 25 euros cinquante.\r\n\r\nClient : Voilà.\r\n\r\nEmployé : Merci monsieur, au revoir.\r\n\r\nClient : Merci, bonne journée.', 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2008/03/boulangerie.jpg', 'http://www.podcastfrancaisfacile.com/wp-content/uploads/files/boulangerie1.mp3', 1457016778, 1, NULL, 0, 1457487910, 1, 0, 23);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ff_videos`
--
ALTER TABLE `ff_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_category_id` (`category_id`),
  ADD KEY `index_inspector_id` (`inspector_id`),
  ADD KEY `index_created_by` (`created_by`),
  ADD KEY `index_updated_by` (`updated_by`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ff_videos`
--
ALTER TABLE `ff_videos`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ff_videos`
--
ALTER TABLE `ff_videos`
  ADD CONSTRAINT `fk_categories_id` FOREIGN KEY (`category_id`) REFERENCES `ff_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_inspector_id` FOREIGN KEY (`inspector_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
